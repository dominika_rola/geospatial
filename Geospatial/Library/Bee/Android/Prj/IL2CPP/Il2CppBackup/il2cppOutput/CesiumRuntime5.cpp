﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.GameObject>
struct Action_1_tFF0F3D982F6CEB68CBA322555CBBEE6AE1D2519C;
// System.Func`2<System.Object,System.Object>
struct Func_2_tACBF5A1656250800CE861707354491F0611F6624;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// CesiumForUnity.Cesium3DTileset[]
struct Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.Action
struct Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07;
// CesiumForUnity.Cesium3DTileset
struct Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA;
// CesiumForUnity.CesiumPointCloudShading
struct CesiumPointCloudShading_t0A4B38FD790EAFF27C00F7BC17FAC74DB4634CA5;
// CesiumForUnity.CesiumRasterOverlay
struct CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6;
// CesiumForUnity.CesiumTileExcluder
struct CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC;
// CesiumForUnity.CesiumTileMapServiceRasterOverlay
struct CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2;
// CesiumForUnity.CesiumWebMapServiceRasterOverlay
struct CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB;
// UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B;
// CesiumForUnity.Helpers
struct Helpers_tF67CB24E1B3D10F6E551B7D1DC5ED497AC3E1DD2;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// CesiumForUnity.NativeCoroutine
struct NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34;
// CesiumForUnity.NativeDownloadHandler
struct NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091;
// System.NotImplementedException
struct NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7;
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct SafeHandleZeroOrMinusOneIsInvalid_tC152552D137451170B3B1A304227B0ECADB65629;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// System.String
struct String_t;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// CesiumForUnity.Cesium3DTileset/ImplementationHandle
struct ImplementationHandle_t685BC708FB0C8850A500FC041F737C2710BD78FC;
// CesiumForUnity.Cesium3DTileset/TilesetLoadFailureDelegate
struct TilesetLoadFailureDelegate_t6E00F7291205D117E7E5713F818BCEB6A898C0F9;
// CesiumForUnity.CesiumRasterOverlay/RasterOverlayLoadFailureDelegate
struct RasterOverlayLoadFailureDelegate_t46B22A97C2253043583D4F95F5C25CD7E1FC4A28;
// CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle
struct ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD;
// CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle
struct ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59;
// CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2
struct U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1;
// CesiumForUnity.NativeDownloadHandler/ImplementationHandle
struct ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6;

IL2CPP_EXTERN_C RuntimeClass* BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral48E8AB7247D551A29855167A89CC4504D9A5C00B;
IL2CPP_EXTERN_C String_t* _stringLiteral5642CA1AC4A79EA83539EBB6D8B5E1410413E219;
IL2CPP_EXTERN_C String_t* _stringLiteral862F749F7B5B6DBD89A8EA5A639585899358F536;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* CesiumTileMapServiceRasterOverlay_AddToTileset_m599BEED2D3FA0E07382C7701588F69BE3AF73378_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CesiumTileMapServiceRasterOverlay_RemoveFromTileset_m972E3917D80448731E8A78F0B7957EA8FF492296_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CesiumWebMapServiceRasterOverlay_AddToTileset_m8536A1B6442E5F55E21D907844A475C36664B2CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CesiumWebMapServiceRasterOverlay_RemoveFromTileset_m4363EE36DD61D1399C010730170646DDDCCDF097_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeDownloadHandler_ReceiveDataNative_mE2D779C1623184305ABC205471ADFE68142C8FBF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE8E016962F281593E81FBD2CD898B99EC0D7E49F_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct Il2CppArrayBounds;

// CesiumForUnity.CesiumWgs84Ellipsoid
struct CesiumWgs84Ellipsoid_t076C1F2DCBA3A70489523831F9696B0465E7FABC  : public RuntimeObject
{
};

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t1DCAB623CAEA6529A96F5F3EDE3C7048A6E313C9  : public RuntimeObject
{
};

// CesiumForUnity.Helpers
struct Helpers_tF67CB24E1B3D10F6E551B7D1DC5ED497AC3E1DD2  : public RuntimeObject
{
};

// CesiumForUnity.NativeCoroutine
struct NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34  : public RuntimeObject
{
	// System.Func`2<System.Object,System.Object> CesiumForUnity.NativeCoroutine::_callback
	Func_2_tACBF5A1656250800CE861707354491F0611F6624* ____callback_0;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// CesiumForUnity.UnityLifetime
struct UnityLifetime_tA6BD8D9A89A5E6F0A1CB4F2530D94249A8651CBA  : public RuntimeObject
{
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2
struct U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1  : public RuntimeObject
{
	// System.Int32 CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// CesiumForUnity.NativeCoroutine CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::<>4__this
	NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* ___U3CU3E4__this_2;
	// System.Object CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::<sentinel>5__2
	RuntimeObject* ___U3CsentinelU3E5__2_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.Int64
struct Int64_t092CFB123BE63C28ACDAF65C68F21A526050DBA3 
{
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Matrix4x4
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;
};

struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix_17;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// Unity.Mathematics.double3
struct double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 
{
	// System.Double Unity.Mathematics.double3::x
	double ___x_0;
	// System.Double Unity.Mathematics.double3::y
	double ___y_1;
	// System.Double Unity.Mathematics.double3::z
	double ___z_2;
};

struct double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4_StaticFields
{
	// Unity.Mathematics.double3 Unity.Mathematics.double3::zero
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___zero_3;
};

// Unity.Mathematics.double4
struct double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 
{
	// System.Double Unity.Mathematics.double4::x
	double ___x_0;
	// System.Double Unity.Mathematics.double4::y
	double ___y_1;
	// System.Double Unity.Mathematics.double4::z
	double ___z_2;
	// System.Double Unity.Mathematics.double4::w
	double ___w_3;
};

struct double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5_StaticFields
{
	// Unity.Mathematics.double4 Unity.Mathematics.double4::zero
	double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___zero_4;
};

// Unity.Mathematics.float3
struct float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E 
{
	// System.Single Unity.Mathematics.float3::x
	float ___x_0;
	// System.Single Unity.Mathematics.float3::y
	float ___y_1;
	// System.Single Unity.Mathematics.float3::z
	float ___z_2;
};

struct float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E_StaticFields
{
	// Unity.Mathematics.float3 Unity.Mathematics.float3::zero
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___zero_3;
};

// Unity.Mathematics.float4
struct float4_t89D9A294E7A79BD81BFBDD18654508532958555E 
{
	// System.Single Unity.Mathematics.float4::x
	float ___x_0;
	// System.Single Unity.Mathematics.float4::y
	float ___y_1;
	// System.Single Unity.Mathematics.float4::z
	float ___z_2;
	// System.Single Unity.Mathematics.float4::w
	float ___w_3;
};

struct float4_t89D9A294E7A79BD81BFBDD18654508532958555E_StaticFields
{
	// Unity.Mathematics.float4 Unity.Mathematics.float4::zero
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___zero_4;
};

// System.Nullable`1<Unity.Mathematics.double3>
struct Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8 
{
	// System.Boolean System.Nullable`1::hasValue
	bool ___hasValue_0;
	// T System.Nullable`1::value
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___value_1;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7  : public CriticalFinalizerObject_t1DCAB623CAEA6529A96F5F3EDE3C7048A6E313C9
{
	// System.IntPtr System.Runtime.InteropServices.SafeHandle::handle
	intptr_t ___handle_0;
	// System.Int32 System.Runtime.InteropServices.SafeHandle::_state
	int32_t ____state_1;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_ownsHandle
	bool ____ownsHandle_2;
	// System.Boolean System.Runtime.InteropServices.SafeHandle::_fullyInitialized
	bool ____fullyInitialized_3;
};

// Unity.Mathematics.double3x3
struct double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 
{
	// Unity.Mathematics.double3 Unity.Mathematics.double3x3::c0
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c0_0;
	// Unity.Mathematics.double3 Unity.Mathematics.double3x3::c1
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c1_1;
	// Unity.Mathematics.double3 Unity.Mathematics.double3x3::c2
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c2_2;
};

struct double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0_StaticFields
{
	// Unity.Mathematics.double3x3 Unity.Mathematics.double3x3::identity
	double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___identity_3;
	// Unity.Mathematics.double3x3 Unity.Mathematics.double3x3::zero
	double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___zero_4;
};

// Unity.Mathematics.double4x4
struct double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C 
{
	// Unity.Mathematics.double4 Unity.Mathematics.double4x4::c0
	double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c0_0;
	// Unity.Mathematics.double4 Unity.Mathematics.double4x4::c1
	double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c1_1;
	// Unity.Mathematics.double4 Unity.Mathematics.double4x4::c2
	double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c2_2;
	// Unity.Mathematics.double4 Unity.Mathematics.double4x4::c3
	double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c3_3;
};

struct double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C_StaticFields
{
	// Unity.Mathematics.double4x4 Unity.Mathematics.double4x4::identity
	double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C ___identity_4;
	// Unity.Mathematics.double4x4 Unity.Mathematics.double4x4::zero
	double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C ___zero_5;
};

// Unity.Mathematics.float3x3
struct float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 
{
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c0
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c0_0;
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c1
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c1_1;
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c2
	float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c2_2;
};

struct float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79_StaticFields
{
	// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::identity
	float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___identity_3;
	// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::zero
	float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___zero_4;
};

// Unity.Mathematics.quaternion
struct quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 
{
	// Unity.Mathematics.float4 Unity.Mathematics.quaternion::value
	float4_t89D9A294E7A79BD81BFBDD18654508532958555E ___value_0;
};

struct quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4_StaticFields
{
	// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::identity
	quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 ___identity_1;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B  : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB
{
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B_marshaled_pinvoke : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B_marshaled_com : public DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct SafeHandleZeroOrMinusOneIsInvalid_tC152552D137451170B3B1A304227B0ECADB65629  : public SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Func`2<System.Object,System.Object>
struct Func_2_tACBF5A1656250800CE861707354491F0611F6624  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// CesiumForUnity.NativeDownloadHandler
struct NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091  : public DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B
{
	// CesiumForUnity.NativeDownloadHandler/ImplementationHandle CesiumForUnity.NativeDownloadHandler::_implementation
	ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* ____implementation_1;
};

// System.NotImplementedException
struct NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle
struct ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD  : public SafeHandleZeroOrMinusOneIsInvalid_tC152552D137451170B3B1A304227B0ECADB65629
{
};

// CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle
struct ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59  : public SafeHandleZeroOrMinusOneIsInvalid_tC152552D137451170B3B1A304227B0ECADB65629
{
};

// CesiumForUnity.NativeDownloadHandler/ImplementationHandle
struct ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6  : public SafeHandleZeroOrMinusOneIsInvalid_tC152552D137451170B3B1A304227B0ECADB65629
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// CesiumForUnity.Cesium3DTileset
struct Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Action`1<UnityEngine.GameObject> CesiumForUnity.Cesium3DTileset::OnTileGameObjectCreated
	Action_1_tFF0F3D982F6CEB68CBA322555CBBEE6AE1D2519C* ___OnTileGameObjectCreated_5;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_showCreditsOnScreen
	bool ____showCreditsOnScreen_7;
	// CesiumForUnity.CesiumDataSource CesiumForUnity.Cesium3DTileset::_tilesetSource
	int32_t ____tilesetSource_8;
	// System.String CesiumForUnity.Cesium3DTileset::_url
	String_t* ____url_9;
	// System.Int64 CesiumForUnity.Cesium3DTileset::_ionAssetID
	int64_t ____ionAssetID_10;
	// System.String CesiumForUnity.Cesium3DTileset::_ionAccessToken
	String_t* ____ionAccessToken_11;
	// System.Single CesiumForUnity.Cesium3DTileset::_maximumScreenSpaceError
	float ____maximumScreenSpaceError_12;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_preloadAncestors
	bool ____preloadAncestors_13;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_preloadSiblings
	bool ____preloadSiblings_14;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_forbidHoles
	bool ____forbidHoles_15;
	// System.UInt32 CesiumForUnity.Cesium3DTileset::_maximumSimultaneousTileLoads
	uint32_t ____maximumSimultaneousTileLoads_16;
	// System.Int64 CesiumForUnity.Cesium3DTileset::_maximumCachedBytes
	int64_t ____maximumCachedBytes_17;
	// System.UInt32 CesiumForUnity.Cesium3DTileset::_loadingDescendantLimit
	uint32_t ____loadingDescendantLimit_18;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_enableFrustumCulling
	bool ____enableFrustumCulling_19;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_enableFogCulling
	bool ____enableFogCulling_20;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_enforceCulledScreenSpaceError
	bool ____enforceCulledScreenSpaceError_21;
	// System.Single CesiumForUnity.Cesium3DTileset::_culledScreenSpaceError
	float ____culledScreenSpaceError_22;
	// UnityEngine.Material CesiumForUnity.Cesium3DTileset::_opaqueMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ____opaqueMaterial_23;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_generateSmoothNormals
	bool ____generateSmoothNormals_24;
	// CesiumForUnity.CesiumPointCloudShading CesiumForUnity.Cesium3DTileset::_pointCloudShading
	CesiumPointCloudShading_t0A4B38FD790EAFF27C00F7BC17FAC74DB4634CA5* ____pointCloudShading_25;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_suspendUpdate
	bool ____suspendUpdate_26;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_previousSuspendUpdate
	bool ____previousSuspendUpdate_27;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_showTilesInHierarchy
	bool ____showTilesInHierarchy_28;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_updateInEditor
	bool ____updateInEditor_29;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_logSelectionStats
	bool ____logSelectionStats_30;
	// System.Boolean CesiumForUnity.Cesium3DTileset::_createPhysicsMeshes
	bool ____createPhysicsMeshes_31;
	// CesiumForUnity.Cesium3DTileset/ImplementationHandle CesiumForUnity.Cesium3DTileset::_implementation
	ImplementationHandle_t685BC708FB0C8850A500FC041F737C2710BD78FC* ____implementation_32;
};

struct Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_StaticFields
{
	// CesiumForUnity.Cesium3DTileset/TilesetLoadFailureDelegate CesiumForUnity.Cesium3DTileset::OnCesium3DTilesetLoadFailure
	TilesetLoadFailureDelegate_t6E00F7291205D117E7E5713F818BCEB6A898C0F9* ___OnCesium3DTilesetLoadFailure_4;
	// System.Action CesiumForUnity.Cesium3DTileset::OnSetShowCreditsOnScreen
	Action_tD00B0A84D7945E50C2DFFC28EFEE6ED44ED2AD07* ___OnSetShowCreditsOnScreen_6;
};

// CesiumForUnity.CesiumRasterOverlay
struct CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean CesiumForUnity.CesiumRasterOverlay::_showCreditsOnScreen
	bool ____showCreditsOnScreen_5;
	// System.Single CesiumForUnity.CesiumRasterOverlay::_maximumScreenSpaceError
	float ____maximumScreenSpaceError_6;
	// System.Int32 CesiumForUnity.CesiumRasterOverlay::_maximumTextureSize
	int32_t ____maximumTextureSize_7;
	// System.Int32 CesiumForUnity.CesiumRasterOverlay::_maximumSimultaneousTileLoads
	int32_t ____maximumSimultaneousTileLoads_8;
	// System.Int64 CesiumForUnity.CesiumRasterOverlay::_subTileCacheBytes
	int64_t ____subTileCacheBytes_9;
};

struct CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6_StaticFields
{
	// CesiumForUnity.CesiumRasterOverlay/RasterOverlayLoadFailureDelegate CesiumForUnity.CesiumRasterOverlay::OnCesiumRasterOverlayLoadFailure
	RasterOverlayLoadFailureDelegate_t46B22A97C2253043583D4F95F5C25CD7E1FC4A28* ___OnCesiumRasterOverlayLoadFailure_4;
};

// CesiumForUnity.CesiumTileExcluder
struct CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// CesiumForUnity.CesiumTileMapServiceRasterOverlay
struct CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2  : public CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6
{
	// System.String CesiumForUnity.CesiumTileMapServiceRasterOverlay::_url
	String_t* ____url_10;
	// System.Boolean CesiumForUnity.CesiumTileMapServiceRasterOverlay::_specifyZoomLevels
	bool ____specifyZoomLevels_11;
	// System.Int32 CesiumForUnity.CesiumTileMapServiceRasterOverlay::_minimumLevel
	int32_t ____minimumLevel_12;
	// System.Int32 CesiumForUnity.CesiumTileMapServiceRasterOverlay::_maximumLevel
	int32_t ____maximumLevel_13;
	// CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle CesiumForUnity.CesiumTileMapServiceRasterOverlay::_implementation
	ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* ____implementation_14;
};

// CesiumForUnity.CesiumWebMapServiceRasterOverlay
struct CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C  : public CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6
{
	// System.String CesiumForUnity.CesiumWebMapServiceRasterOverlay::_baseUrl
	String_t* ____baseUrl_10;
	// System.String CesiumForUnity.CesiumWebMapServiceRasterOverlay::_layers
	String_t* ____layers_11;
	// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::_tileWidth
	int32_t ____tileWidth_12;
	// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::_tileHeight
	int32_t ____tileHeight_13;
	// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::_minimumLevel
	int32_t ____minimumLevel_14;
	// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::_maximumLevel
	int32_t ____maximumLevel_15;
	// CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle CesiumForUnity.CesiumWebMapServiceRasterOverlay::_implementation
	ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* ____implementation_16;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// CesiumForUnity.Cesium3DTileset[]
struct Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D  : public RuntimeArray
{
	ALIGN_FIELD (8) Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* m_Items[1];

	inline Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Component_GetComponentsInChildren_TisRuntimeObject_m1F5B6FC0689B07D4FAAC0C605D9B2933A9B32543_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void System.Nullable`1<Unity.Mathematics.double3>::.ctor(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E_gshared (Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___value0, const RuntimeMethod* method) ;
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_gshared_inline (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___arg0, const RuntimeMethod* method) ;

// T[] UnityEngine.Component::GetComponentsInChildren<CesiumForUnity.Cesium3DTileset>()
inline Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1F5B6FC0689B07D4FAAC0C605D9B2933A9B32543_gshared)(__this, method);
}
// System.Void CesiumForUnity.CesiumTileExcluder::AddToTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_AddToTileset_m0BCB0C9B2FE2FE4B3B869EB8E99E78583830EF23 (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileExcluder::RemoveFromTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_RemoveFromTileset_mB8C2B223F4C6ED391F77219E76C4AC575F9F4717 (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) ;
// System.Void Reinterop.ReinteropInitializer::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReinteropInitializer_Initialize_mC21B2A7426F23F6D19F90EB4462149EEC23CCF79 (const RuntimeMethod* method) ;
// System.IntPtr Reinterop.ObjectHandleUtility::CreateHandle(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E (RuntimeObject* ___o0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileExcluder::DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset_mA26CFD34ED2F16147825AA156F341D6DA641E5C6 (intptr_t ___thiz0, intptr_t ___tileset1, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileExcluder::DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset_m8734DF270B953FCD638EFA43FFE6014211D700B8 (intptr_t ___thiz0, intptr_t ___tileset1, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumRasterOverlay::Refresh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168 (CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6* __this, const RuntimeMethod* method) ;
// System.Void System.NotImplementedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742 (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* __this, String_t* ___message0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset(System.IntPtr,CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset_m6F396522416A1E7E4198D43A029CBAD32E4DC7A9 (intptr_t ___thiz0, ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset(System.IntPtr,CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset_mD49D86C525AAF5EF0B2486D10BD7E62057AF5AC5 (intptr_t ___thiz0, ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle::.ctor(CesiumForUnity.CesiumTileMapServiceRasterOverlay)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImplementationHandle__ctor_m639BE448B6FF7F0ACD915173645BA90A04CB3FA0 (ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* __this, CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* ___managed0, const RuntimeMethod* method) ;
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeHandle_Dispose_m4FB5B8A7ED78B90757F1B570D4025F3BA26A39F3 (SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DisposeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DisposeImplementation_mB4CF7C6F2CA318C79588547F710F681BAB34AFE3 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumRasterOverlay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumRasterOverlay__ctor_mBF08B99B21CEFD240A84AE711451E8270BCE8FA1 (CesiumRasterOverlay_t215C3071149D6D1FB222192FC3DB34D0723C3FA6* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::CreateImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_CreateImplementation_mCC557FC1C6F43D141D3F03608BA759481F40C973 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) ;
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousAddRef(System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeHandle_DangerousAddRef_m9FA46208A92D8B33059B8E8712F49AE45BB5E922 (SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7* __this, bool* ___success0, const RuntimeMethod* method) ;
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousRelease()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeHandle_DangerousRelease_m30A8B4E5BEA935C8925BC2115CD0AD13B937953E (SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7* __this, const RuntimeMethod* method) ;
// System.Void Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeHandleZeroOrMinusOneIsInvalid__ctor_m9BA85F78EC25654EE170CA999EC379D9A4B59B89 (SafeHandleZeroOrMinusOneIsInvalid_tC152552D137451170B3B1A304227B0ECADB65629* __this, bool ___ownsHandle0, const RuntimeMethod* method) ;
// System.IntPtr CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation_mA476AD27477286AD369C62D0E1FC2E48F5D0EEBF (intptr_t ___thiz0, const RuntimeMethod* method) ;
// System.Void System.Runtime.InteropServices.SafeHandle::SetHandle(System.IntPtr)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SafeHandle_SetHandle_m003D64748F9DFBA1E3C0B23798C23BA81AA21C2A_inline (SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7* __this, intptr_t ___handle0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation_m786E9D4A980EA7A9695BC389D40EFBB2FAD4FD44 (intptr_t ___implementation0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset(System.IntPtr,CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset_mB480679FDE7D92759AA9E376B391BF7B156919E6 (intptr_t ___thiz0, ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset(System.IntPtr,CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset_m1A2D9F6776AD725516EDD7943C0393311D83C5E5 (intptr_t ___thiz0, ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle::.ctor(CesiumForUnity.CesiumWebMapServiceRasterOverlay)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImplementationHandle__ctor_m0DA922A43F88A0C9384EB9E2C0C697FDE330A999 (ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* __this, CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* ___managed0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DisposeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DisposeImplementation_m72AA46F7579DEB9E4D164CDE6EDCC54A0D75DC4A (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::CreateImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_CreateImplementation_m4C0860193FA4213D82AEB4121662E7253C2B2CB2 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) ;
// System.IntPtr CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation_m8C54A643D5AA0555D0E3037E3A975C1C9C5DDD54 (intptr_t ___thiz0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation_mB748C8C3EB397CE220E9FBBE9A2B4896772D7829 (intptr_t ___implementation0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii(Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii_m678096AAA73014B9DCF2EABF67B38ADE1CC4D61A (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 CesiumForUnity.CesiumWgs84Ellipsoid::GetRadii()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 CesiumWgs84Ellipsoid_GetRadii_m10F8E0E42E21410101AB3F22EDFD1426645C43DD (const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::cmax(Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_cmax_mD1CA685960C6D3E73AE61E158449D1F136B2D8D9_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::cmin(Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_cmin_mD62CF2BF7B13402E46E966F3BED814004E5D8C65_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, const RuntimeMethod* method) ;
// System.Byte CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface_m593719466F71E4789AB1B165A587ACBD5B0809A1 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___earthCenteredEarthFixed0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) ;
// System.Void System.Nullable`1<Unity.Mathematics.double3>::.ctor(T)
inline void Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E (Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4, const RuntimeMethod*))Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E_gshared)(__this, ___value0, method);
}
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal_m8438EEC0C21734C52F3E4ECC866D40833412C4E7 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___earthCenteredEarthFixed0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed_mF5B0648E1CC82054D9ADDC8043EE9EF8DA9D8377 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___longitudeLatitudeHeight0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight_mF73D8C75E992EE3265AA399BB86B63427CC7FD81 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___earthCenteredEarthFixed0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.NativeDownloadHandler/ImplementationHandle::.ctor(CesiumForUnity.NativeDownloadHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImplementationHandle__ctor_m07B970AC703BB29D300B6C1873ED2BC975E76143 (ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* __this, NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* ___managed0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.DownloadHandler::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DownloadHandler_Dispose_mD5D4CCF0C2DFF1CB57C9B3A0EF4213ECB9F8F607 (DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.NativeDownloadHandler::DisposeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_DisposeImplementation_m618A868DEE8729DBA9D368FBE03466DE4A803EAA (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Networking.DownloadHandlerScript::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DownloadHandlerScript__ctor_m67B5897E7D6354051F54E8EB70ACA62BA0EF264A (DownloadHandlerScript_t42FD7363F738391BB1EA2552FF18F9FA7C0EE38B* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___preallocatedBuffer0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.NativeDownloadHandler::CreateImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_CreateImplementation_mC4E7F430EFD43A3C5F06DF67865DEF647C7A64CC (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) ;
// System.IntPtr System.IntPtr::op_Explicit(System.Void*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IntPtr_op_Explicit_mE2CEC14C61FD5E2159A03EA2AD97F5CDC5BB9F4D (void* ___value0, const RuntimeMethod* method) ;
// System.Boolean CesiumForUnity.NativeDownloadHandler::ReceiveDataNative(System.IntPtr,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeDownloadHandler_ReceiveDataNative_mE2D779C1623184305ABC205471ADFE68142C8FBF (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, intptr_t ___data0, int32_t ___dataLength1, const RuntimeMethod* method) ;
// System.Byte CesiumForUnity.NativeDownloadHandler::DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative(System.IntPtr,CesiumForUnity.NativeDownloadHandler/ImplementationHandle,System.IntPtr,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative_mAB4ECF15CEBCBF80B4DF152C80C1E31C398B7B5B (intptr_t ___thiz0, ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* ___implementation1, intptr_t ___data2, int32_t ___dataLength3, const RuntimeMethod* method) ;
// System.IntPtr CesiumForUnity.NativeDownloadHandler::DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation_mAF076FB4B78BE6C80021D63D06E57F3F133E4FBE (intptr_t ___thiz0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.NativeDownloadHandler::DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation_m4E09345C0F5E07FBCE8E06E532695440C45BD9B5 (intptr_t ___implementation0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double4x4::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4x4__ctor_mDB1C9BED251AFC0CD16CA1D52545C5A1DAA6878F_inline (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C* __this, double ___m000, double ___m011, double ___m022, double ___m033, double ___m104, double ___m115, double ___m126, double ___m137, double ___m208, double ___m219, double ___m2210, double ___m2311, double ___m3012, double ___m3113, double ___m3214, double ___m3315, const RuntimeMethod* method) ;
// UnityEngine.Vector4 CesiumForUnity.Helpers::FromMathematics(Unity.Mathematics.double4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Helpers_FromMathematics_m480F9CECA74BED4970A73DA27BA2D8126C596BB8 (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___vector0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Matrix4x4__ctor_m6523044D700F15EC6BCD183633A329EE56AA8C99 (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* __this, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___column00, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___column11, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___column22, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___column33, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double3x3::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3x3__ctor_mBEE4C5D1CCF08BD6C8E94DD819F144FBC690E888_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0* __this, double ___m000, double ___m011, double ___m022, double ___m103, double ___m114, double ___m125, double ___m206, double ___m217, double ___m228, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.float3x3::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_m3AB31C9B587ABDCF15C8BF0E3A5B0158996A75ED_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79* __this, float ___m000, float ___m011, float ___m022, float ___m103, float ___m114, float ___m125, float ___m206, float ___m217, float ___m228, const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::length(Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_length_m936CF76FF0C94E358B2193CFB59E41080B87E641_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::op_Division(Unity.Mathematics.double3,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Division_mBFCCDD798F735189AE8D843BD014FCF5F1EEAD93_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double ___rhs1, const RuntimeMethod* method) ;
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Explicit(Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E float3_op_Explicit_mC39F75EB64FD16249FAD573FD8B6ADB14F132D78_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___v0, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.float3,Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_mA652DC011B892B36A8216646B51B2014F89CE93E_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79* __this, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c00, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c11, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c22, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double3::.ctor(System.Double,System.Double,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* __this, double ___x0, double ___y1, double ___z2, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.math::cross(Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 math_cross_mD4DDFE34A1DA411148681014E59AEDC0655C0973_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___y1, const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::dot(Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_dot_m710CE5F525FC4891265B265568DE10C0100B509B_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___y1, const RuntimeMethod* method) ;
// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Multiply(Unity.Mathematics.float3x3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 float3x3_op_Multiply_mF3B9F7F790D87EFB7EBC38F26ABDC9305816484A_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___lhs0, float ___rhs1, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::op_Multiply(Unity.Mathematics.double3,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double ___rhs1, const RuntimeMethod* method) ;
// Unity.Mathematics.quaternion Unity.Mathematics.math::quaternion(Unity.Mathematics.float3x3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 math_quaternion_mE9DBDC1E38A93968B447FF4D365823A7889B0749_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___m0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double4::get_xyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* __this, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double3x3::.ctor(Unity.Mathematics.double3,Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3x3__ctor_m0BF27C1E4D2C1C4965521A8B3A919CF9DB11B305_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c00, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c11, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c22, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.Helpers::MatrixToRotationAndScale(Unity.Mathematics.double3x3,Unity.Mathematics.quaternion&,Unity.Mathematics.double3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_MatrixToRotationAndScale_mA7BA8F572035E772AB8B038C797831E630BDAC46 (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___matrix0, quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4* ___rotation1, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___scale2, const RuntimeMethod* method) ;
// UnityEngine.Quaternion Unity.Mathematics.quaternion::op_Implicit(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 quaternion_op_Implicit_m78F07E28F5AB9C26F0EE997B2F12AF4A209FCD41 (quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 ___q0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 Unity.Mathematics.float3::op_Implicit(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 float3_op_Implicit_m9CC301DFD67EEFAA15CA05E91913E862B22326F6 (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___v0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 CesiumForUnity.Helpers::FromMathematics(Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Helpers_FromMathematics_m8CB96E41C8AFC98D3FBDBD51283C7A77D4655409 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___vector0, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.Helpers::MatrixToInaccurateRotationAndScale(Unity.Mathematics.double3x3,UnityEngine.Quaternion&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_MatrixToInaccurateRotationAndScale_m47E8DEAED109886AABCC62F11B18860885F58EC6 (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___matrix0, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* ___rotation1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___scale2, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3x3__ctor_mF94488DFF7867CFC89648E024FA89A19F23E2FAE (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79* __this, quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 ___q0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3x3 Unity.Mathematics.double3x3::op_Implicit(Unity.Mathematics.float3x3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 double3x3_op_Implicit_mDE9DBCF7F737C1128250D072AF94867314B7FAA1_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___v0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3x3 Unity.Mathematics.math::mul(Unity.Mathematics.double3x3,Unity.Mathematics.double3x3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 math_mul_m8006A1F722590AD2791FB2B506A1A74A0816494F_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___a0, double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___b1, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double4::.ctor(Unity.Mathematics.double3,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4__ctor_mAAB30D5A18E63BBEB9AD9B98E95D510784E64B26_inline (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___xyz0, double ___w1, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double4x4::.ctor(Unity.Mathematics.double4,Unity.Mathematics.double4,Unity.Mathematics.double4,Unity.Mathematics.double4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4x4__ctor_mC28DA9877244770A1E61E41A50DF95F405AFD34B_inline (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C* __this, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c00, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c11, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c22, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c33, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetEnumeratorU3Ed__2__ctor_m33BA9725115B0FB505A76B96DF2E1D9827F32492 (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
inline RuntimeObject* Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_inline (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___arg0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Func_2_tACBF5A1656250800CE861707354491F0611F6624*, RuntimeObject*, const RuntimeMethod*))Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_gshared_inline)(__this, ___arg0, method);
}
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::max(System.Double,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_max_m8830F8721EFC73BCF991CD497115A103B86BF3BE_inline (double ___x0, double ___y1, const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::min(System.Double,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_min_m29A6A5FB36524D911D13DDB4866FF005C7BF00D5_inline (double ___x0, double ___y1, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double4::.ctor(System.Double,System.Double,System.Double,System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4__ctor_m49D96B66F7E9E5F0783AA40FCBE7EC199F5C7C42_inline (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* __this, double ___x0, double ___y1, double ___z2, double ___w3, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mC61002CD0EC13D7C37D846D021A78C028FB80DB9_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Double Unity.Mathematics.math::sqrt(System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_sqrt_mA3A9D5DFDF6841F8836E3ECD5D83555842383F36_inline (double ___x0, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.float3::.ctor(Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mD7BFFAB3D7057D71DB7B2F5A50788D197E1AA49B_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___v0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::get_yzx()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_get_yzx_mFEFD36EE9E6E6470EDDCF595DEAAB85FCBAC2795_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* __this, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::op_Multiply(Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Multiply_mFF3B33CAB54AB767C1B7927B97658C307150BCA9_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___rhs1, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::op_Subtraction(Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Subtraction_m22E94C140DA02DCD57ADB54B6DEEFA271AEB82A0_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___rhs1, const RuntimeMethod* method) ;
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E float3_op_Multiply_m6E5DC552C8B0F9A180298BD9197FF47B14E0EA81_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___lhs0, float ___rhs1, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.quaternion::.ctor(Unity.Mathematics.float3x3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void quaternion__ctor_m354F09C0E50CA59DA43037E9993EAE9BF97E9120 (quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4* __this, float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___m0, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double3x3::.ctor(Unity.Mathematics.float3x3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3x3__ctor_m4A89254CD6C32BCF5BCEBC60A4E712E2360DD972_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0* __this, float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___v0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::op_Addition(Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___rhs1, const RuntimeMethod* method) ;
// Unity.Mathematics.double3x3 Unity.Mathematics.math::double3x3(Unity.Mathematics.double3,Unity.Mathematics.double3,Unity.Mathematics.double3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 math_double3x3_mDEDFD3D5E0FAD4EC0550DE55A86A7D199B3CC61B_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c00, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c11, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c22, const RuntimeMethod* method) ;
// System.Boolean System.Double::IsNaN(System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Double_IsNaN_mF2BC6D1FD4813179B2CAE58D29770E42830D0883_inline (double ___d0, const RuntimeMethod* method) ;
// Unity.Mathematics.double3 Unity.Mathematics.double3::op_Implicit(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Implicit_m5DCE807570E8C929820AE8D221FFEE4861D9D5D9_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___v0, const RuntimeMethod* method) ;
// System.Int64 System.BitConverter::DoubleToInt64Bits(System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t BitConverter_DoubleToInt64Bits_m4F42741818550F9956B5FBAF88C051F4DE5B0AE6_inline (double ___value0, const RuntimeMethod* method) ;
// System.Void Unity.Mathematics.double3::.ctor(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3__ctor_m246C72AEDE9AC7E52CF7DF7FEE065D66EF96AB8A_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* __this, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___v0, const RuntimeMethod* method) ;
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset(intptr_t, intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset(intptr_t, intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C intptr_t CDECL DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset(intptr_t, void*, intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset(intptr_t, void*, intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C intptr_t CDECL DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset(intptr_t, void*, intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset(intptr_t, void*, intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C uint8_t CDECL DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C intptr_t CDECL DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C void CDECL DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
IL2CPP_EXTERN_C uint8_t CDECL DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative(intptr_t, void*, intptr_t, int32_t);
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.CesiumTileExcluder::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_OnEnable_mFF4A13B7E736F32BEE62818292AB6E2A4B42DDA6 (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* V_0 = NULL;
	int32_t V_1 = 0;
	Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* V_2 = NULL;
	{
		// Cesium3DTileset[] tilesets = this.GetComponentsInChildren<Cesium3DTileset>();
		Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* L_0;
		L_0 = Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F(__this, Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F_RuntimeMethod_var);
		// foreach (Cesium3DTileset tileset in tilesets)
		V_0 = L_0;
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		// foreach (Cesium3DTileset tileset in tilesets)
		Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// this.AddToTileset(tileset);
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_5 = V_2;
		CesiumTileExcluder_AddToTileset_m0BCB0C9B2FE2FE4B3B869EB8E99E78583830EF23(__this, L_5, NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, 1));
	}

IL_001a:
	{
		// foreach (Cesium3DTileset tileset in tilesets)
		int32_t L_7 = V_1;
		Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileExcluder::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_OnDisable_m1B3F4456992703073011ED2B21BEAD10FF78718D (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* V_0 = NULL;
	int32_t V_1 = 0;
	Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* V_2 = NULL;
	{
		// Cesium3DTileset[] tilesets = this.GetComponentsInChildren<Cesium3DTileset>();
		Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* L_0;
		L_0 = Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F(__this, Component_GetComponentsInChildren_TisCesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA_mDCF7749C29EDEB48A678C6F570CC357C0B3DC91F_RuntimeMethod_var);
		// foreach (Cesium3DTileset tileset in tilesets)
		V_0 = L_0;
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		// foreach (Cesium3DTileset tileset in tilesets)
		Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// this.RemoveFromTileset(tileset);
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_5 = V_2;
		CesiumTileExcluder_RemoveFromTileset_mB8C2B223F4C6ED391F77219E76C4AC575F9F4717(__this, L_5, NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, 1));
	}

IL_001a:
	{
		// foreach (Cesium3DTileset tileset in tilesets)
		int32_t L_7 = V_1;
		Cesium3DTilesetU5BU5D_t20C7BD2D6878B90AA36C5F4123785F341966A56D* L_8 = V_0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileExcluder::AddToTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_AddToTileset_m0BCB0C9B2FE2FE4B3B869EB8E99E78583830EF23 (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Reinterop.ReinteropInitializer.Initialize();
		il2cpp_codegen_runtime_class_init_inline(ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		ReinteropInitializer_Initialize_mC21B2A7426F23F6D19F90EB4462149EEC23CCF79(NULL);
		// DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset(Reinterop.ObjectHandleUtility.CreateHandle(this), Reinterop.ObjectHandleUtility.CreateHandle(tileset));
		intptr_t L_0;
		L_0 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_1 = ___tileset0;
		intptr_t L_2;
		L_2 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_1, NULL);
		CesiumTileExcluder_DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset_mA26CFD34ED2F16147825AA156F341D6DA641E5C6(L_0, L_2, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileExcluder::RemoveFromTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_RemoveFromTileset_mB8C2B223F4C6ED391F77219E76C4AC575F9F4717 (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Reinterop.ReinteropInitializer.Initialize();
		il2cpp_codegen_runtime_class_init_inline(ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		ReinteropInitializer_Initialize_mC21B2A7426F23F6D19F90EB4462149EEC23CCF79(NULL);
		// DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset(Reinterop.ObjectHandleUtility.CreateHandle(this), Reinterop.ObjectHandleUtility.CreateHandle(tileset));
		intptr_t L_0;
		L_0 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_1 = ___tileset0;
		intptr_t L_2;
		L_2 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_1, NULL);
		CesiumTileExcluder_DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset_m8734DF270B953FCD638EFA43FFE6014211D700B8(L_0, L_2, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileExcluder::DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset_mA26CFD34ED2F16147825AA156F341D6DA641E5C6 (intptr_t ___thiz0, intptr_t ___tileset1, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t, intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumTileExcluder_AddToTileset)(___thiz0, ___tileset1);
	#else
	il2cppPInvokeFunc(___thiz0, ___tileset1);
	#endif

}
// System.Void CesiumForUnity.CesiumTileExcluder::DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder_DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset_m8734DF270B953FCD638EFA43FFE6014211D700B8 (intptr_t ___thiz0, intptr_t ___tileset1, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t, intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumTileExcluder_RemoveFromTileset)(___thiz0, ___tileset1);
	#else
	il2cppPInvokeFunc(___thiz0, ___tileset1);
	#endif

}
// System.Void CesiumForUnity.CesiumTileExcluder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileExcluder__ctor_m14966A6BCD048E362AD35BE8A985F59242D8200B (CesiumTileExcluder_tF02993434E73D2A34893E5364363EFDD5078D4EC* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String CesiumForUnity.CesiumTileMapServiceRasterOverlay::get_url()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CesiumTileMapServiceRasterOverlay_get_url_mA3D90B423BF78649EB2D867D67F54F6E5D0D7FAF (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// get => this._url;
		String_t* L_0 = __this->____url_10;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::set_url(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_set_url_m6F575110A4C4E659FC0D4F499DE4A8CF861A89F8 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		// this._url = value;
		String_t* L_0 = ___value0;
		__this->____url_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____url_10), (void*)L_0);
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Boolean CesiumForUnity.CesiumTileMapServiceRasterOverlay::get_specifyZoomLevels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CesiumTileMapServiceRasterOverlay_get_specifyZoomLevels_m791B9557F009F4888E20D9E748DD78A52A0F672A (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// get => this._specifyZoomLevels;
		bool L_0 = __this->____specifyZoomLevels_11;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::set_specifyZoomLevels(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_set_specifyZoomLevels_mFA6D5C93ACF7CA9C9339DAEFA8B4FD60988BBE1F (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, bool ___value0, const RuntimeMethod* method) 
{
	{
		// this._specifyZoomLevels = value;
		bool L_0 = ___value0;
		__this->____specifyZoomLevels_11 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Int32 CesiumForUnity.CesiumTileMapServiceRasterOverlay::get_minimumLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CesiumTileMapServiceRasterOverlay_get_minimumLevel_m7DC6E298F3C460C63A25E8D0091E7E09E904D6F5 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// get => this._minimumLevel;
		int32_t L_0 = __this->____minimumLevel_12;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::set_minimumLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_set_minimumLevel_m2FB9DC98996C1141A9314A4ABE4B5029E5A05C29 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// this._minimumLevel = value;
		int32_t L_0 = ___value0;
		__this->____minimumLevel_12 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Int32 CesiumForUnity.CesiumTileMapServiceRasterOverlay::get_maximumLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CesiumTileMapServiceRasterOverlay_get_maximumLevel_m4C57B561BC7ABA0A9763D5787098AD2ED7FF5A14 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// get => this._maximumLevel;
		int32_t L_0 = __this->____maximumLevel_13;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::set_maximumLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_set_maximumLevel_m78118F71A490D905C39F69509D6AF6789DA5B613 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// this._maximumLevel = value;
		int32_t L_0 = ___value0;
		__this->____maximumLevel_13 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::AddToTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_AddToTileset_m599BEED2D3FA0E07382C7701588F69BE3AF73378 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) 
{
	{
		// if (this._implementation == null || this._implementation.IsInvalid)
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_0 = __this->____implementation_14;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_1 = __this->____implementation_14;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (!L_2)
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		// throw new NotImplementedException("The native implementation is missing so AddToTileset cannot be invoked. This may be caused by a missing call to CreateImplementation in one of your constructors, or it may be that the entire native implementation shared library is missing or out of date.");
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_3 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral5642CA1AC4A79EA83539EBB6D8B5E1410413E219)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CesiumTileMapServiceRasterOverlay_AddToTileset_m599BEED2D3FA0E07382C7701588F69BE3AF73378_RuntimeMethod_var)));
	}

IL_0020:
	{
		// DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset(Reinterop.ObjectHandleUtility.CreateHandle(this), _implementation, Reinterop.ObjectHandleUtility.CreateHandle(tileset));
		intptr_t L_4;
		L_4 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_5 = __this->____implementation_14;
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_6 = ___tileset0;
		intptr_t L_7;
		L_7 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_6, NULL);
		CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset_m6F396522416A1E7E4198D43A029CBAD32E4DC7A9(L_4, L_5, L_7, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::RemoveFromTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_RemoveFromTileset_m972E3917D80448731E8A78F0B7957EA8FF492296 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) 
{
	{
		// if (this._implementation == null || this._implementation.IsInvalid)
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_0 = __this->____implementation_14;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_1 = __this->____implementation_14;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (!L_2)
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		// throw new NotImplementedException("The native implementation is missing so RemoveFromTileset cannot be invoked. This may be caused by a missing call to CreateImplementation in one of your constructors, or it may be that the entire native implementation shared library is missing or out of date.");
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_3 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral48E8AB7247D551A29855167A89CC4504D9A5C00B)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CesiumTileMapServiceRasterOverlay_RemoveFromTileset_m972E3917D80448731E8A78F0B7957EA8FF492296_RuntimeMethod_var)));
	}

IL_0020:
	{
		// DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset(Reinterop.ObjectHandleUtility.CreateHandle(this), _implementation, Reinterop.ObjectHandleUtility.CreateHandle(tileset));
		intptr_t L_4;
		L_4 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_5 = __this->____implementation_14;
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_6 = ___tileset0;
		intptr_t L_7;
		L_7 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_6, NULL);
		CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset_mD49D86C525AAF5EF0B2486D10BD7E62057AF5AC5(L_4, L_5, L_7, NULL);
		// }
		return;
	}
}
// CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle CesiumForUnity.CesiumTileMapServiceRasterOverlay::get_NativeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* CesiumTileMapServiceRasterOverlay_get_NativeImplementation_m5AC211029B05776F7E16F2CFA8403D4B4F978348 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// get { return _implementation; }
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_0 = __this->____implementation_14;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::CreateImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_CreateImplementation_mCC557FC1C6F43D141D3F03608BA759481F40C973 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Reinterop.ReinteropInitializer.Initialize();
		il2cpp_codegen_runtime_class_init_inline(ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		ReinteropInitializer_Initialize_mC21B2A7426F23F6D19F90EB4462149EEC23CCF79(NULL);
		// this._implementation = new ImplementationHandle(this);
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_0 = (ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD*)il2cpp_codegen_object_new(ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ImplementationHandle__ctor_m639BE448B6FF7F0ACD915173645BA90A04CB3FA0(L_0, __this, NULL);
		__this->____implementation_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____implementation_14), (void*)L_0);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DisposeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DisposeImplementation_mB4CF7C6F2CA318C79588547F710F681BAB34AFE3 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// if (this._implementation != null && !this._implementation.IsInvalid)
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_0 = __this->____implementation_14;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_1 = __this->____implementation_14;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		// this._implementation.Dispose();
		ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* L_3 = __this->____implementation_14;
		NullCheck(L_3);
		SafeHandle_Dispose_m4FB5B8A7ED78B90757F1B570D4025F3BA26A39F3(L_3, NULL);
	}

IL_0020:
	{
		// this._implementation = null;
		__this->____implementation_14 = (ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____implementation_14), (void*)(ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD*)NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_Dispose_m36689D5180347F585C8D2C2D24E551C09C4A3113 (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	{
		// this.DisposeImplementation();
		CesiumTileMapServiceRasterOverlay_DisposeImplementation_mB4CF7C6F2CA318C79588547F710F681BAB34AFE3(__this, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay__ctor_m13205A8D3C47824F0774F710E02E50E2A3B3F42B (CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string _url = "";
		__this->____url_10 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____url_10), (void*)_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// private int _maximumLevel = 10;
		__this->____maximumLevel_13 = ((int32_t)10);
		// public CesiumTileMapServiceRasterOverlay()
		CesiumRasterOverlay__ctor_mBF08B99B21CEFD240A84AE711451E8270BCE8FA1(__this, NULL);
		// CreateImplementation();
		CesiumTileMapServiceRasterOverlay_CreateImplementation_mCC557FC1C6F43D141D3F03608BA759481F40C973(__this, NULL);
		// }
		return;
	}
}
// System.IntPtr CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation_mA476AD27477286AD369C62D0E1FC2E48F5D0EEBF (intptr_t ___thiz0, const RuntimeMethod* method) 
{
	typedef intptr_t (CDECL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation)(___thiz0);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(___thiz0);
	#endif

	return returnValue;
}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation_m786E9D4A980EA7A9695BC389D40EFBB2FAD4FD44 (intptr_t ___implementation0, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation)(___implementation0);
	#else
	il2cppPInvokeFunc(___implementation0);
	#endif

}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset(System.IntPtr,CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset_m6F396522416A1E7E4198D43A029CBAD32E4DC7A9 (intptr_t ___thiz0, ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t, void*, intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___implementation1' to native representation
	void* ____implementation1_marshaled = NULL;
	if (___implementation1 == NULL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("implementation"), NULL);
	bool ___safeHandle_reference_incremented_for____implementation1 = false;
	SafeHandle_DangerousAddRef_m9FA46208A92D8B33059B8E8712F49AE45BB5E922(___implementation1, (&___safeHandle_reference_incremented_for____implementation1), NULL);
	____implementation1_marshaled = reinterpret_cast<void*>((___implementation1)->___handle_0);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_AddToTileset)(___thiz0, ____implementation1_marshaled, ___tileset2);
	#else
	il2cppPInvokeFunc(___thiz0, ____implementation1_marshaled, ___tileset2);
	#endif

	// Marshaling cleanup of parameter '___implementation1' native representation
	if (___safeHandle_reference_incremented_for____implementation1)
	{
		SafeHandle_DangerousRelease_m30A8B4E5BEA935C8925BC2115CD0AD13B937953E(___implementation1, NULL);
	}

}
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset(System.IntPtr,CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset_mD49D86C525AAF5EF0B2486D10BD7E62057AF5AC5 (intptr_t ___thiz0, ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t, void*, intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___implementation1' to native representation
	void* ____implementation1_marshaled = NULL;
	if (___implementation1 == NULL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("implementation"), NULL);
	bool ___safeHandle_reference_incremented_for____implementation1 = false;
	SafeHandle_DangerousAddRef_m9FA46208A92D8B33059B8E8712F49AE45BB5E922(___implementation1, (&___safeHandle_reference_incremented_for____implementation1), NULL);
	____implementation1_marshaled = reinterpret_cast<void*>((___implementation1)->___handle_0);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_RemoveFromTileset)(___thiz0, ____implementation1_marshaled, ___tileset2);
	#else
	il2cppPInvokeFunc(___thiz0, ____implementation1_marshaled, ___tileset2);
	#endif

	// Marshaling cleanup of parameter '___implementation1' native representation
	if (___safeHandle_reference_incremented_for____implementation1)
	{
		SafeHandle_DangerousRelease_m30A8B4E5BEA935C8925BC2115CD0AD13B937953E(___implementation1, NULL);
	}

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle::.ctor(CesiumForUnity.CesiumTileMapServiceRasterOverlay)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImplementationHandle__ctor_m639BE448B6FF7F0ACD915173645BA90A04CB3FA0 (ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* __this, CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* ___managed0, const RuntimeMethod* method) 
{
	{
		// public ImplementationHandle(CesiumTileMapServiceRasterOverlay managed) : base(true)
		SafeHandleZeroOrMinusOneIsInvalid__ctor_m9BA85F78EC25654EE170CA999EC379D9A4B59B89(__this, (bool)1, NULL);
		// SetHandle(DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation(Reinterop.ObjectHandleUtility.CreateHandle(managed)));
		CesiumTileMapServiceRasterOverlay_t1F481EFD3D3A33A76BCB77EC506D7F63DF404AC2* L_0 = ___managed0;
		intptr_t L_1;
		L_1 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_0, NULL);
		intptr_t L_2;
		L_2 = CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_CreateImplementation_mA476AD27477286AD369C62D0E1FC2E48F5D0EEBF(L_1, NULL);
		SafeHandle_SetHandle_m003D64748F9DFBA1E3C0B23798C23BA81AA21C2A_inline(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Boolean CesiumForUnity.CesiumTileMapServiceRasterOverlay/ImplementationHandle::ReleaseHandle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImplementationHandle_ReleaseHandle_m507421BB1C8AE94FBA67058C1B7F7920548F90C4 (ImplementationHandle_t15013B1F8138540C8B2BD19EF83A6D9A759D16FD* __this, const RuntimeMethod* method) 
{
	{
		// DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation(this.handle);
		intptr_t L_0 = ((SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7*)__this)->___handle_0;
		CesiumTileMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumTileMapServiceRasterOverlay_DestroyImplementation_m786E9D4A980EA7A9695BC389D40EFBB2FAD4FD44(L_0, NULL);
		// return true;
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_baseUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CesiumWebMapServiceRasterOverlay_get_baseUrl_m428CDE8D19FD6288B3D8C7170CCB18B95DE92C86 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get => this._baseUrl;
		String_t* L_0 = __this->____baseUrl_10;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::set_baseUrl(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_set_baseUrl_m1187905A3A45CB2DA62E84BEE51C5B3A787BB45E (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		// this._baseUrl = value;
		String_t* L_0 = ___value0;
		__this->____baseUrl_10 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____baseUrl_10), (void*)L_0);
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.String CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_layers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CesiumWebMapServiceRasterOverlay_get_layers_m5790E71B0C9DEFE7E5178F58F8B38524DF7EA748 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get => this._layers;
		String_t* L_0 = __this->____layers_11;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::set_layers(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_set_layers_m47E7FD4991C4725E238B10BBD972A7DCA4B4F3F0 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		// this._layers = value;
		String_t* L_0 = ___value0;
		__this->____layers_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____layers_11), (void*)L_0);
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_tileWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CesiumWebMapServiceRasterOverlay_get_tileWidth_mD77847313E64AC4F0FD5C6A61E8B19A1BF6EB3F0 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get => this._tileWidth;
		int32_t L_0 = __this->____tileWidth_12;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::set_tileWidth(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_set_tileWidth_m675C115D64D4800BC268FC06D04649EE3719D4BA (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// this._tileWidth = value;
		int32_t L_0 = ___value0;
		__this->____tileWidth_12 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_tileHeight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CesiumWebMapServiceRasterOverlay_get_tileHeight_m3E5B11102C3DF748E68E8170CFD5933BFDF45497 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get => this._tileHeight;
		int32_t L_0 = __this->____tileHeight_13;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::set_tileHeight(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_set_tileHeight_m8EFB1BB63C57D8EDB972D947A656DB6DA54B9FAD (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// this._tileHeight = value;
		int32_t L_0 = ___value0;
		__this->____tileHeight_13 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_minimumLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CesiumWebMapServiceRasterOverlay_get_minimumLevel_m8641AD764BED9AD89D8F1C64379A8846F7E6290E (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get => this._minimumLevel;
		int32_t L_0 = __this->____minimumLevel_14;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::set_minimumLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_set_minimumLevel_m644F7660404DF19CC475C965BC7B41A5B89F70EB (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// this._minimumLevel = value;
		int32_t L_0 = ___value0;
		__this->____minimumLevel_14 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Int32 CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_maximumLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CesiumWebMapServiceRasterOverlay_get_maximumLevel_mF08915714AC067D8B4A5A74E04C67FE12D925B92 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get => this._maximumLevel;
		int32_t L_0 = __this->____maximumLevel_15;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::set_maximumLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_set_maximumLevel_m975A6FCFB66A9652910DD81BCCD67B7A1CFBF660 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, int32_t ___value0, const RuntimeMethod* method) 
{
	{
		// this._maximumLevel = value;
		int32_t L_0 = ___value0;
		__this->____maximumLevel_15 = L_0;
		// this.Refresh();
		CesiumRasterOverlay_Refresh_mB368EFFDB8862E09B85CE8716B13F6F0B75AA168(__this, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::AddToTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_AddToTileset_m8536A1B6442E5F55E21D907844A475C36664B2CE (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) 
{
	{
		// if (this._implementation == null || this._implementation.IsInvalid)
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_0 = __this->____implementation_16;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_1 = __this->____implementation_16;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (!L_2)
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		// throw new NotImplementedException("The native implementation is missing so AddToTileset cannot be invoked. This may be caused by a missing call to CreateImplementation in one of your constructors, or it may be that the entire native implementation shared library is missing or out of date.");
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_3 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral5642CA1AC4A79EA83539EBB6D8B5E1410413E219)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CesiumWebMapServiceRasterOverlay_AddToTileset_m8536A1B6442E5F55E21D907844A475C36664B2CE_RuntimeMethod_var)));
	}

IL_0020:
	{
		// DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset(Reinterop.ObjectHandleUtility.CreateHandle(this), _implementation, Reinterop.ObjectHandleUtility.CreateHandle(tileset));
		intptr_t L_4;
		L_4 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_5 = __this->____implementation_16;
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_6 = ___tileset0;
		intptr_t L_7;
		L_7 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_6, NULL);
		CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset_mB480679FDE7D92759AA9E376B391BF7B156919E6(L_4, L_5, L_7, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::RemoveFromTileset(CesiumForUnity.Cesium3DTileset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_RemoveFromTileset_m4363EE36DD61D1399C010730170646DDDCCDF097 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* ___tileset0, const RuntimeMethod* method) 
{
	{
		// if (this._implementation == null || this._implementation.IsInvalid)
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_0 = __this->____implementation_16;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_1 = __this->____implementation_16;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (!L_2)
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		// throw new NotImplementedException("The native implementation is missing so RemoveFromTileset cannot be invoked. This may be caused by a missing call to CreateImplementation in one of your constructors, or it may be that the entire native implementation shared library is missing or out of date.");
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_3 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral48E8AB7247D551A29855167A89CC4504D9A5C00B)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&CesiumWebMapServiceRasterOverlay_RemoveFromTileset_m4363EE36DD61D1399C010730170646DDDCCDF097_RuntimeMethod_var)));
	}

IL_0020:
	{
		// DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset(Reinterop.ObjectHandleUtility.CreateHandle(this), _implementation, Reinterop.ObjectHandleUtility.CreateHandle(tileset));
		intptr_t L_4;
		L_4 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_5 = __this->____implementation_16;
		Cesium3DTileset_t5498D56ED52ABDDEE4DD3708E3D23458923859FA* L_6 = ___tileset0;
		intptr_t L_7;
		L_7 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_6, NULL);
		CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset_m1A2D9F6776AD725516EDD7943C0393311D83C5E5(L_4, L_5, L_7, NULL);
		// }
		return;
	}
}
// CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle CesiumForUnity.CesiumWebMapServiceRasterOverlay::get_NativeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* CesiumWebMapServiceRasterOverlay_get_NativeImplementation_mADDAE0F7EA8DCDB6F9B07DBA56E3F94CEB05228D (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// get { return _implementation; }
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_0 = __this->____implementation_16;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::CreateImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_CreateImplementation_m4C0860193FA4213D82AEB4121662E7253C2B2CB2 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Reinterop.ReinteropInitializer.Initialize();
		il2cpp_codegen_runtime_class_init_inline(ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		ReinteropInitializer_Initialize_mC21B2A7426F23F6D19F90EB4462149EEC23CCF79(NULL);
		// this._implementation = new ImplementationHandle(this);
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_0 = (ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59*)il2cpp_codegen_object_new(ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ImplementationHandle__ctor_m0DA922A43F88A0C9384EB9E2C0C697FDE330A999(L_0, __this, NULL);
		__this->____implementation_16 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____implementation_16), (void*)L_0);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DisposeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DisposeImplementation_m72AA46F7579DEB9E4D164CDE6EDCC54A0D75DC4A (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// if (this._implementation != null && !this._implementation.IsInvalid)
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_0 = __this->____implementation_16;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_1 = __this->____implementation_16;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		// this._implementation.Dispose();
		ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* L_3 = __this->____implementation_16;
		NullCheck(L_3);
		SafeHandle_Dispose_m4FB5B8A7ED78B90757F1B570D4025F3BA26A39F3(L_3, NULL);
	}

IL_0020:
	{
		// this._implementation = null;
		__this->____implementation_16 = (ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____implementation_16), (void*)(ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59*)NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_Dispose_m8D6A038C4FBB42E3ADE9FDA4831097020DAB4997 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	{
		// this.DisposeImplementation();
		CesiumWebMapServiceRasterOverlay_DisposeImplementation_m72AA46F7579DEB9E4D164CDE6EDCC54A0D75DC4A(__this, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay__ctor_mD5F9C32A48396D1A8CD5B556C38AD6D474B985A3 (CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private string _baseUrl = "";
		__this->____baseUrl_10 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____baseUrl_10), (void*)_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// private string _layers = "";
		__this->____layers_11 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____layers_11), (void*)_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// private int _tileWidth = 256;
		__this->____tileWidth_12 = ((int32_t)256);
		// private int _tileHeight = 256;
		__this->____tileHeight_13 = ((int32_t)256);
		// private int _maximumLevel = 14;
		__this->____maximumLevel_15 = ((int32_t)14);
		// public CesiumWebMapServiceRasterOverlay()
		CesiumRasterOverlay__ctor_mBF08B99B21CEFD240A84AE711451E8270BCE8FA1(__this, NULL);
		// CreateImplementation();
		CesiumWebMapServiceRasterOverlay_CreateImplementation_m4C0860193FA4213D82AEB4121662E7253C2B2CB2(__this, NULL);
		// }
		return;
	}
}
// System.IntPtr CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation_m8C54A643D5AA0555D0E3037E3A975C1C9C5DDD54 (intptr_t ___thiz0, const RuntimeMethod* method) 
{
	typedef intptr_t (CDECL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation)(___thiz0);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(___thiz0);
	#endif

	return returnValue;
}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation_mB748C8C3EB397CE220E9FBBE9A2B4896772D7829 (intptr_t ___implementation0, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation)(___implementation0);
	#else
	il2cppPInvokeFunc(___implementation0);
	#endif

}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset(System.IntPtr,CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset_mB480679FDE7D92759AA9E376B391BF7B156919E6 (intptr_t ___thiz0, ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t, void*, intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___implementation1' to native representation
	void* ____implementation1_marshaled = NULL;
	if (___implementation1 == NULL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("implementation"), NULL);
	bool ___safeHandle_reference_incremented_for____implementation1 = false;
	SafeHandle_DangerousAddRef_m9FA46208A92D8B33059B8E8712F49AE45BB5E922(___implementation1, (&___safeHandle_reference_incremented_for____implementation1), NULL);
	____implementation1_marshaled = reinterpret_cast<void*>((___implementation1)->___handle_0);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_AddToTileset)(___thiz0, ____implementation1_marshaled, ___tileset2);
	#else
	il2cppPInvokeFunc(___thiz0, ____implementation1_marshaled, ___tileset2);
	#endif

	// Marshaling cleanup of parameter '___implementation1' native representation
	if (___safeHandle_reference_incremented_for____implementation1)
	{
		SafeHandle_DangerousRelease_m30A8B4E5BEA935C8925BC2115CD0AD13B937953E(___implementation1, NULL);
	}

}
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay::DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset(System.IntPtr,CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset_m1A2D9F6776AD725516EDD7943C0393311D83C5E5 (intptr_t ___thiz0, ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* ___implementation1, intptr_t ___tileset2, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t, void*, intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___implementation1' to native representation
	void* ____implementation1_marshaled = NULL;
	if (___implementation1 == NULL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("implementation"), NULL);
	bool ___safeHandle_reference_incremented_for____implementation1 = false;
	SafeHandle_DangerousAddRef_m9FA46208A92D8B33059B8E8712F49AE45BB5E922(___implementation1, (&___safeHandle_reference_incremented_for____implementation1), NULL);
	____implementation1_marshaled = reinterpret_cast<void*>((___implementation1)->___handle_0);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_RemoveFromTileset)(___thiz0, ____implementation1_marshaled, ___tileset2);
	#else
	il2cppPInvokeFunc(___thiz0, ____implementation1_marshaled, ___tileset2);
	#endif

	// Marshaling cleanup of parameter '___implementation1' native representation
	if (___safeHandle_reference_incremented_for____implementation1)
	{
		SafeHandle_DangerousRelease_m30A8B4E5BEA935C8925BC2115CD0AD13B937953E(___implementation1, NULL);
	}

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle::.ctor(CesiumForUnity.CesiumWebMapServiceRasterOverlay)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImplementationHandle__ctor_m0DA922A43F88A0C9384EB9E2C0C697FDE330A999 (ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* __this, CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* ___managed0, const RuntimeMethod* method) 
{
	{
		// public ImplementationHandle(CesiumWebMapServiceRasterOverlay managed) : base(true)
		SafeHandleZeroOrMinusOneIsInvalid__ctor_m9BA85F78EC25654EE170CA999EC379D9A4B59B89(__this, (bool)1, NULL);
		// SetHandle(DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation(Reinterop.ObjectHandleUtility.CreateHandle(managed)));
		CesiumWebMapServiceRasterOverlay_tAC8E1DB962910E2BD316ADEDF330D700184E662C* L_0 = ___managed0;
		intptr_t L_1;
		L_1 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_0, NULL);
		intptr_t L_2;
		L_2 = CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_CreateImplementation_m8C54A643D5AA0555D0E3037E3A975C1C9C5DDD54(L_1, NULL);
		SafeHandle_SetHandle_m003D64748F9DFBA1E3C0B23798C23BA81AA21C2A_inline(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Boolean CesiumForUnity.CesiumWebMapServiceRasterOverlay/ImplementationHandle::ReleaseHandle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImplementationHandle_ReleaseHandle_m74017DF6755947C6CED32CE6196581C324413D1C (ImplementationHandle_t82F3C7DEC6674D08AA6A625530780417D8B48E59* __this, const RuntimeMethod* method) 
{
	{
		// DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation(this.handle);
		intptr_t L_0 = ((SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7*)__this)->___handle_0;
		CesiumWebMapServiceRasterOverlay_DotNet_CesiumForUnity_CesiumWebMapServiceRasterOverlay_DestroyImplementation_mB748C8C3EB397CE220E9FBBE9A2B4896772D7829(L_0, NULL);
		// return true;
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Mathematics.double3 CesiumForUnity.CesiumWgs84Ellipsoid::GetRadii()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 CesiumWgs84Ellipsoid_GetRadii_m10F8E0E42E21410101AB3F22EDFD1426645C43DD (const RuntimeMethod* method) 
{
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var returnValue = new Unity.Mathematics.double3();
		il2cpp_codegen_initobj((&V_0), sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4));
		// DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii(&returnValue);
		CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii_m678096AAA73014B9DCF2EABF67B38ADE1CC4D61A((double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&V_0)), NULL);
		// return returnValue;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = V_0;
		return L_0;
	}
}
// System.Double CesiumForUnity.CesiumWgs84Ellipsoid::GetMaximumRadius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double CesiumWgs84Ellipsoid_GetMaximumRadius_mAC46B7C010A616C1C451B4FA4FA5AC74CD2CEB32 (const RuntimeMethod* method) 
{
	{
		// return math.cmax(CesiumWgs84Ellipsoid.GetRadii());
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0;
		L_0 = CesiumWgs84Ellipsoid_GetRadii_m10F8E0E42E21410101AB3F22EDFD1426645C43DD(NULL);
		double L_1;
		L_1 = math_cmax_mD1CA685960C6D3E73AE61E158449D1F136B2D8D9_inline(L_0, NULL);
		return L_1;
	}
}
// System.Double CesiumForUnity.CesiumWgs84Ellipsoid::GetMinimumRadius()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double CesiumWgs84Ellipsoid_GetMinimumRadius_m4A64EF6A390B917F2FE8F1BA694D3206CDB6A2DA (const RuntimeMethod* method) 
{
	{
		// return math.cmin(CesiumWgs84Ellipsoid.GetRadii());
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0;
		L_0 = CesiumWgs84Ellipsoid_GetRadii_m10F8E0E42E21410101AB3F22EDFD1426645C43DD(NULL);
		double L_1;
		L_1 = math_cmin_mD62CF2BF7B13402E46E966F3BED814004E5D8C65_inline(L_0, NULL);
		return L_1;
	}
}
// System.Nullable`1<Unity.Mathematics.double3> CesiumForUnity.CesiumWgs84Ellipsoid::ScaleToGeodeticSurface(Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8 CesiumWgs84Ellipsoid_ScaleToGeodeticSurface_m08B146D48F8F71E0166878912AD2CD073D212C57 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___earthCenteredEarthFixed0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var returnValue = new Unity.Mathematics.double3();
		il2cpp_codegen_initobj((&V_0), sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4));
		// var result = DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface(&earthCenteredEarthFixed, &returnValue);
		uint8_t L_0;
		L_0 = CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface_m593719466F71E4789AB1B165A587ACBD5B0809A1((double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&___earthCenteredEarthFixed0)), (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&V_0)), NULL);
		// return result == 1 ? returnValue : null;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8));
		Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8 L_1 = V_1;
		return L_1;
	}

IL_0020:
	{
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = V_0;
		Nullable_1_t292B6499B4FB064453057DDA8BEED95AAE5424D8 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E((&L_3), L_2, /*hidden argument*/Nullable_1__ctor_mA6909A447FCEBF13D2C07AF3F2104AFE9167A93E_RuntimeMethod_var);
		return L_3;
	}
}
// Unity.Mathematics.double3 CesiumForUnity.CesiumWgs84Ellipsoid::GeodeticSurfaceNormal(Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 CesiumWgs84Ellipsoid_GeodeticSurfaceNormal_m2A8AD10F5660C33A7CF43469CA09726F70E984B9 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___earthCenteredEarthFixed0, const RuntimeMethod* method) 
{
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var returnValue = new Unity.Mathematics.double3();
		il2cpp_codegen_initobj((&V_0), sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4));
		// DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal(&earthCenteredEarthFixed, &returnValue);
		CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal_m8438EEC0C21734C52F3E4ECC866D40833412C4E7((double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&___earthCenteredEarthFixed0)), (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&V_0)), NULL);
		// return returnValue;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = V_0;
		return L_0;
	}
}
// Unity.Mathematics.double3 CesiumForUnity.CesiumWgs84Ellipsoid::LongitudeLatitudeHeightToEarthCenteredEarthFixed(Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed_m6998B8AC075149178533D1C516FA95F8AA04EEA7 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___longitudeLatitudeHeight0, const RuntimeMethod* method) 
{
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var returnValue = new Unity.Mathematics.double3();
		il2cpp_codegen_initobj((&V_0), sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4));
		// DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed(&longitudeLatitudeHeight, &returnValue);
		CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed_mF5B0648E1CC82054D9ADDC8043EE9EF8DA9D8377((double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&___longitudeLatitudeHeight0)), (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&V_0)), NULL);
		// return returnValue;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = V_0;
		return L_0;
	}
}
// Unity.Mathematics.double3 CesiumForUnity.CesiumWgs84Ellipsoid::EarthCenteredEarthFixedToLongitudeLatitudeHeight(Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight_mBD814A88230A66A8651170CD363EE449BB144C12 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___earthCenteredEarthFixed0, const RuntimeMethod* method) 
{
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var returnValue = new Unity.Mathematics.double3();
		il2cpp_codegen_initobj((&V_0), sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4));
		// DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight(&earthCenteredEarthFixed, &returnValue);
		CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight_mF73D8C75E992EE3265AA399BB86B63427CC7FD81((double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&___earthCenteredEarthFixed0)), (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)((uintptr_t)(&V_0)), NULL);
		// return returnValue;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = V_0;
		return L_0;
	}
}
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii(Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii_m678096AAA73014B9DCF2EABF67B38ADE1CC4D61A (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue0, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GetRadii)(___pReturnValue0);
	#else
	il2cppPInvokeFunc(___pReturnValue0);
	#endif

}
// System.Byte CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface_m593719466F71E4789AB1B165A587ACBD5B0809A1 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___earthCenteredEarthFixed0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) 
{
	typedef uint8_t (CDECL *PInvokeFunc) (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*) + sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	uint8_t returnValue = reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_ScaleToGeodeticSurface)(___earthCenteredEarthFixed0, ___pReturnValue1);
	#else
	uint8_t returnValue = il2cppPInvokeFunc(___earthCenteredEarthFixed0, ___pReturnValue1);
	#endif

	return returnValue;
}
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal_m8438EEC0C21734C52F3E4ECC866D40833412C4E7 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___earthCenteredEarthFixed0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*) + sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_GeodeticSurfaceNormal)(___earthCenteredEarthFixed0, ___pReturnValue1);
	#else
	il2cppPInvokeFunc(___earthCenteredEarthFixed0, ___pReturnValue1);
	#endif

}
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed_mF5B0648E1CC82054D9ADDC8043EE9EF8DA9D8377 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___longitudeLatitudeHeight0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*) + sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_LongitudeLatitudeHeightToEarthCenteredEarthFixed)(___longitudeLatitudeHeight0, ___pReturnValue1);
	#else
	il2cppPInvokeFunc(___longitudeLatitudeHeight0, ___pReturnValue1);
	#endif

}
// System.Void CesiumForUnity.CesiumWgs84Ellipsoid::DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight(Unity.Mathematics.double3*,Unity.Mathematics.double3*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CesiumWgs84Ellipsoid_DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight_mF73D8C75E992EE3265AA399BB86B63427CC7FD81 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___earthCenteredEarthFixed0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___pReturnValue1, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*) + sizeof(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_CesiumWgs84Ellipsoid_EarthCenteredEarthFixedToLongitudeLatitudeHeight)(___earthCenteredEarthFixed0, ___pReturnValue1);
	#else
	il2cppPInvokeFunc(___earthCenteredEarthFixed0, ___pReturnValue1);
	#endif

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CesiumForUnity.NativeDownloadHandler/ImplementationHandle CesiumForUnity.NativeDownloadHandler::get_NativeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* NativeDownloadHandler_get_NativeImplementation_m371EAC14A719C137F0FB8453568DC9FDD9EFE24A (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) 
{
	{
		// get { return _implementation; }
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_0 = __this->____implementation_1;
		return L_0;
	}
}
// System.Void CesiumForUnity.NativeDownloadHandler::CreateImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_CreateImplementation_mC4E7F430EFD43A3C5F06DF67865DEF647C7A64CC (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Reinterop.ReinteropInitializer.Initialize();
		il2cpp_codegen_runtime_class_init_inline(ReinteropInitializer_t4EA3C28134472D46B8485711894D636A4477E487_il2cpp_TypeInfo_var);
		ReinteropInitializer_Initialize_mC21B2A7426F23F6D19F90EB4462149EEC23CCF79(NULL);
		// this._implementation = new ImplementationHandle(this);
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_0 = (ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6*)il2cpp_codegen_object_new(ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		ImplementationHandle__ctor_m07B970AC703BB29D300B6C1873ED2BC975E76143(L_0, __this, NULL);
		__this->____implementation_1 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____implementation_1), (void*)L_0);
		// }
		return;
	}
}
// System.Void CesiumForUnity.NativeDownloadHandler::DisposeImplementation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_DisposeImplementation_m618A868DEE8729DBA9D368FBE03466DE4A803EAA (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) 
{
	{
		// if (this._implementation != null && !this._implementation.IsInvalid)
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_0 = __this->____implementation_1;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_1 = __this->____implementation_1;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		// this._implementation.Dispose();
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_3 = __this->____implementation_1;
		NullCheck(L_3);
		SafeHandle_Dispose_m4FB5B8A7ED78B90757F1B570D4025F3BA26A39F3(L_3, NULL);
	}

IL_0020:
	{
		// this._implementation = null;
		__this->____implementation_1 = (ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____implementation_1), (void*)(ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6*)NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.NativeDownloadHandler::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_Dispose_m0BEE19D9E50054B8BB21635CFDE09CC918092FA6 (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) 
{
	{
		// base.Dispose();
		DownloadHandler_Dispose_mD5D4CCF0C2DFF1CB57C9B3A0EF4213ECB9F8F607(__this, NULL);
		// this.DisposeImplementation();
		NativeDownloadHandler_DisposeImplementation_m618A868DEE8729DBA9D368FBE03466DE4A803EAA(__this, NULL);
		// }
		return;
	}
}
// System.IntPtr CesiumForUnity.NativeDownloadHandler::DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation_mAF076FB4B78BE6C80021D63D06E57F3F133E4FBE (intptr_t ___thiz0, const RuntimeMethod* method) 
{
	typedef intptr_t (CDECL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation)(___thiz0);
	#else
	intptr_t returnValue = il2cppPInvokeFunc(___thiz0);
	#endif

	return returnValue;
}
// System.Void CesiumForUnity.NativeDownloadHandler::DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation_m4E09345C0F5E07FBCE8E06E532695440C45BD9B5 (intptr_t ___implementation0, const RuntimeMethod* method) 
{
	typedef void (CDECL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation)(___implementation0);
	#else
	il2cppPInvokeFunc(___implementation0);
	#endif

}
// System.Byte CesiumForUnity.NativeDownloadHandler::DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative(System.IntPtr,CesiumForUnity.NativeDownloadHandler/ImplementationHandle,System.IntPtr,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint8_t NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative_mAB4ECF15CEBCBF80B4DF152C80C1E31C398B7B5B (intptr_t ___thiz0, ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* ___implementation1, intptr_t ___data2, int32_t ___dataLength3, const RuntimeMethod* method) 
{
	typedef uint8_t (CDECL *PInvokeFunc) (intptr_t, void*, intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("CesiumForUnityNative-Runtime"), "DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative", IL2CPP_CALL_C, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Marshaling of parameter '___implementation1' to native representation
	void* ____implementation1_marshaled = NULL;
	if (___implementation1 == NULL) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_argument_null_exception("implementation"), NULL);
	bool ___safeHandle_reference_incremented_for____implementation1 = false;
	SafeHandle_DangerousAddRef_m9FA46208A92D8B33059B8E8712F49AE45BB5E922(___implementation1, (&___safeHandle_reference_incremented_for____implementation1), NULL);
	____implementation1_marshaled = reinterpret_cast<void*>((___implementation1)->___handle_0);

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_CesiumForUnityNative_Runtime_INTERNAL
	uint8_t returnValue = reinterpret_cast<PInvokeFunc>(DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative)(___thiz0, ____implementation1_marshaled, ___data2, ___dataLength3);
	#else
	uint8_t returnValue = il2cppPInvokeFunc(___thiz0, ____implementation1_marshaled, ___data2, ___dataLength3);
	#endif

	// Marshaling cleanup of parameter '___implementation1' native representation
	if (___safeHandle_reference_incremented_for____implementation1)
	{
		SafeHandle_DangerousRelease_m30A8B4E5BEA935C8925BC2115CD0AD13B937953E(___implementation1, NULL);
	}

	return returnValue;
}
// System.Void CesiumForUnity.NativeDownloadHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDownloadHandler__ctor_m1EF7F8652BA26CB5538E8B6C5A852FC1F31CE34C (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// : base(new byte[16384])
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16384));
		DownloadHandlerScript__ctor_m67B5897E7D6354051F54E8EB70ACA62BA0EF264A(__this, L_0, NULL);
		// CreateImplementation();
		NativeDownloadHandler_CreateImplementation_mC4E7F430EFD43A3C5F06DF67865DEF647C7A64CC(__this, NULL);
		// }
		return;
	}
}
// System.Boolean CesiumForUnity.NativeDownloadHandler::ReceiveData(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeDownloadHandler_ReceiveData_mE09DFD8D9329D426AF1A3A7B0668A9EEE2BD01B7 (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___data0, int32_t ___dataLength1, const RuntimeMethod* method) 
{
	uint8_t* V_0 = NULL;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_1 = NULL;
	{
		// fixed(byte* p = data)
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = ___data0;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = L_0;
		V_1 = L_1;
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_2 = V_1;
		NullCheck(L_2);
		if (((int32_t)(((RuntimeArray*)L_2)->max_length)))
		{
			goto IL_000f;
		}
	}

IL_000a:
	{
		V_0 = (uint8_t*)((uintptr_t)0);
		goto IL_0018;
	}

IL_000f:
	{
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = V_1;
		NullCheck(L_3);
		V_0 = (uint8_t*)((uintptr_t)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
	}

IL_0018:
	{
		// bool result = this.ReceiveDataNative((IntPtr)p, dataLength);
		uint8_t* L_4 = V_0;
		intptr_t L_5;
		L_5 = IntPtr_op_Explicit_mE2CEC14C61FD5E2159A03EA2AD97F5CDC5BB9F4D((void*)L_4, NULL);
		int32_t L_6 = ___dataLength1;
		bool L_7;
		L_7 = NativeDownloadHandler_ReceiveDataNative_mE2D779C1623184305ABC205471ADFE68142C8FBF(__this, L_5, L_6, NULL);
		// return result;
		return L_7;
	}
}
// System.Boolean CesiumForUnity.NativeDownloadHandler::ReceiveDataNative(System.IntPtr,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeDownloadHandler_ReceiveDataNative_mE2D779C1623184305ABC205471ADFE68142C8FBF (NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* __this, intptr_t ___data0, int32_t ___dataLength1, const RuntimeMethod* method) 
{
	{
		// if (this._implementation == null || this._implementation.IsInvalid)
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_0 = __this->____implementation_1;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_1 = __this->____implementation_1;
		NullCheck(L_1);
		bool L_2;
		L_2 = VirtualFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, L_1);
		if (!L_2)
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		// throw new NotImplementedException("The native implementation is missing so ReceiveDataNative cannot be invoked. This may be caused by a missing call to CreateImplementation in one of your constructors, or it may be that the entire native implementation shared library is missing or out of date.");
		NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8* L_3 = (NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t6366FE4DCF15094C51F4833B91A2AE68D4DA90E8_il2cpp_TypeInfo_var)));
		NullCheck(L_3);
		NotImplementedException__ctor_m8339D1A685E8D77CAC9D3260C06B38B5C7CA7742(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral862F749F7B5B6DBD89A8EA5A639585899358F536)), NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NativeDownloadHandler_ReceiveDataNative_mE2D779C1623184305ABC205471ADFE68142C8FBF_RuntimeMethod_var)));
	}

IL_0020:
	{
		// var result = DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative(Reinterop.ObjectHandleUtility.CreateHandle(this), _implementation, data, dataLength);
		intptr_t L_4;
		L_4 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(__this, NULL);
		ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* L_5 = __this->____implementation_1;
		intptr_t L_6 = ___data0;
		int32_t L_7 = ___dataLength1;
		uint8_t L_8;
		L_8 = NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_ReceiveDataNative_mAB4ECF15CEBCBF80B4DF152C80C1E31C398B7B5B(L_4, L_5, L_6, L_7, NULL);
		// return result != 0;
		return (bool)((!(((uint32_t)L_8) <= ((uint32_t)0)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.NativeDownloadHandler/ImplementationHandle::.ctor(CesiumForUnity.NativeDownloadHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ImplementationHandle__ctor_m07B970AC703BB29D300B6C1873ED2BC975E76143 (ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* __this, NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* ___managed0, const RuntimeMethod* method) 
{
	{
		// public ImplementationHandle(NativeDownloadHandler managed) : base(true)
		SafeHandleZeroOrMinusOneIsInvalid__ctor_m9BA85F78EC25654EE170CA999EC379D9A4B59B89(__this, (bool)1, NULL);
		// SetHandle(DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation(Reinterop.ObjectHandleUtility.CreateHandle(managed)));
		NativeDownloadHandler_t407A6A51C207FA98FCAF956597D270E3CECF3091* L_0 = ___managed0;
		intptr_t L_1;
		L_1 = ObjectHandleUtility_CreateHandle_mB79C62851E53353881B5E0377965190872787A0E(L_0, NULL);
		intptr_t L_2;
		L_2 = NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_CreateImplementation_mAF076FB4B78BE6C80021D63D06E57F3F133E4FBE(L_1, NULL);
		SafeHandle_SetHandle_m003D64748F9DFBA1E3C0B23798C23BA81AA21C2A_inline(__this, L_2, NULL);
		// }
		return;
	}
}
// System.Boolean CesiumForUnity.NativeDownloadHandler/ImplementationHandle::ReleaseHandle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ImplementationHandle_ReleaseHandle_mBD14B95F317788C5E36ED87A016043656071CDC7 (ImplementationHandle_t4B1675EF30A8FFA9AEEC0C5EEBC72592176309D6* __this, const RuntimeMethod* method) 
{
	{
		// DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation(this.handle);
		intptr_t L_0 = ((SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7*)__this)->___handle_0;
		NativeDownloadHandler_DotNet_CesiumForUnity_NativeDownloadHandler_DestroyImplementation_m4E09345C0F5E07FBCE8E06E532695440C45BD9B5(L_0, NULL);
		// return true;
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector3 CesiumForUnity.Helpers::FromMathematics(Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Helpers_FromMathematics_m8CB96E41C8AFC98D3FBDBD51283C7A77D4655409 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___vector0, const RuntimeMethod* method) 
{
	{
		// return new Vector3((float)vector.x, (float)vector.y, (float)vector.z);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___vector0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___vector0;
		double L_3 = L_2.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___vector0;
		double L_5 = L_4.___z_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_6), ((float)L_1), ((float)L_3), ((float)L_5), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector4 CesiumForUnity.Helpers::FromMathematics(Unity.Mathematics.double4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 Helpers_FromMathematics_m480F9CECA74BED4970A73DA27BA2D8126C596BB8 (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___vector0, const RuntimeMethod* method) 
{
	{
		// return new Vector4((float)vector.x, (float)vector.y, (float)vector.z, (float)vector.w);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_0 = ___vector0;
		double L_1 = L_0.___x_0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_2 = ___vector0;
		double L_3 = L_2.___y_1;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_4 = ___vector0;
		double L_5 = L_4.___z_2;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_6 = ___vector0;
		double L_7 = L_6.___w_3;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_8), ((float)L_1), ((float)L_3), ((float)L_5), ((float)L_7), /*hidden argument*/NULL);
		return L_8;
	}
}
// Unity.Mathematics.double4x4 CesiumForUnity.Helpers::ToMathematics(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C Helpers_ToMathematics_m93248C8886F21E2C39A5DD66B5F3A44E0BBF0ABD (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___matrix0, const RuntimeMethod* method) 
{
	{
		// return new double4x4(
		//     matrix.m00, matrix.m01, matrix.m02, matrix.m03,
		//     matrix.m10, matrix.m11, matrix.m12, matrix.m13,
		//     matrix.m20, matrix.m21, matrix.m22, matrix.m23,
		//     matrix.m30, matrix.m31, matrix.m32, matrix.m33);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0 = ___matrix0;
		float L_1 = L_0.___m00_0;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_2 = ___matrix0;
		float L_3 = L_2.___m01_4;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_4 = ___matrix0;
		float L_5 = L_4.___m02_8;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_6 = ___matrix0;
		float L_7 = L_6.___m03_12;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_8 = ___matrix0;
		float L_9 = L_8.___m10_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_10 = ___matrix0;
		float L_11 = L_10.___m11_5;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_12 = ___matrix0;
		float L_13 = L_12.___m12_9;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_14 = ___matrix0;
		float L_15 = L_14.___m13_13;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_16 = ___matrix0;
		float L_17 = L_16.___m20_2;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_18 = ___matrix0;
		float L_19 = L_18.___m21_6;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_20 = ___matrix0;
		float L_21 = L_20.___m22_10;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_22 = ___matrix0;
		float L_23 = L_22.___m23_14;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_24 = ___matrix0;
		float L_25 = L_24.___m30_3;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_26 = ___matrix0;
		float L_27 = L_26.___m31_7;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_28 = ___matrix0;
		float L_29 = L_28.___m32_11;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_30 = ___matrix0;
		float L_31 = L_30.___m33_15;
		double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C L_32;
		memset((&L_32), 0, sizeof(L_32));
		double4x4__ctor_mDB1C9BED251AFC0CD16CA1D52545C5A1DAA6878F_inline((&L_32), ((double)L_1), ((double)L_3), ((double)L_5), ((double)L_7), ((double)L_9), ((double)L_11), ((double)L_13), ((double)L_15), ((double)L_17), ((double)L_19), ((double)L_21), ((double)L_23), ((double)L_25), ((double)L_27), ((double)L_29), ((double)L_31), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Matrix4x4 CesiumForUnity.Helpers::FromMathematics(Unity.Mathematics.double4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Helpers_FromMathematics_m75193037A87DE03FFDF4C5047BC19EF64F70C038 (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C ___matrix0, const RuntimeMethod* method) 
{
	{
		// return new Matrix4x4(FromMathematics(matrix.c0), FromMathematics(matrix.c1), FromMathematics(matrix.c2), FromMathematics(matrix.c3));
		double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C L_0 = ___matrix0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_1 = L_0.___c0_0;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_2;
		L_2 = Helpers_FromMathematics_m480F9CECA74BED4970A73DA27BA2D8126C596BB8(L_1, NULL);
		double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C L_3 = ___matrix0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_4 = L_3.___c1_1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_5;
		L_5 = Helpers_FromMathematics_m480F9CECA74BED4970A73DA27BA2D8126C596BB8(L_4, NULL);
		double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C L_6 = ___matrix0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_7 = L_6.___c2_2;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_8;
		L_8 = Helpers_FromMathematics_m480F9CECA74BED4970A73DA27BA2D8126C596BB8(L_7, NULL);
		double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C L_9 = ___matrix0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_10 = L_9.___c3_3;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_11;
		L_11 = Helpers_FromMathematics_m480F9CECA74BED4970A73DA27BA2D8126C596BB8(L_10, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Matrix4x4__ctor_m6523044D700F15EC6BCD183633A329EE56AA8C99((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.double3x3 CesiumForUnity.Helpers::ToMathematicsDouble3x3(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 Helpers_ToMathematicsDouble3x3_m1F80F0DD792DD2610FF628FB6BF90D94158E8367 (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___matrix0, const RuntimeMethod* method) 
{
	{
		// return new double3x3(
		//     matrix.m00, matrix.m01, matrix.m02,
		//     matrix.m10, matrix.m11, matrix.m12,
		//     matrix.m20, matrix.m21, matrix.m22);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0 = ___matrix0;
		float L_1 = L_0.___m00_0;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_2 = ___matrix0;
		float L_3 = L_2.___m01_4;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_4 = ___matrix0;
		float L_5 = L_4.___m02_8;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_6 = ___matrix0;
		float L_7 = L_6.___m10_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_8 = ___matrix0;
		float L_9 = L_8.___m11_5;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_10 = ___matrix0;
		float L_11 = L_10.___m12_9;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_12 = ___matrix0;
		float L_13 = L_12.___m20_2;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_14 = ___matrix0;
		float L_15 = L_14.___m21_6;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_16 = ___matrix0;
		float L_17 = L_16.___m22_10;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_18;
		memset((&L_18), 0, sizeof(L_18));
		double3x3__ctor_mBEE4C5D1CCF08BD6C8E94DD819F144FBC690E888_inline((&L_18), ((double)L_1), ((double)L_3), ((double)L_5), ((double)L_7), ((double)L_9), ((double)L_11), ((double)L_13), ((double)L_15), ((double)L_17), /*hidden argument*/NULL);
		return L_18;
	}
}
// Unity.Mathematics.float3x3 CesiumForUnity.Helpers::ToMathematicsFloat3x3(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 Helpers_ToMathematicsFloat3x3_mF27E1692D7A87E7AA0DA230A59DF0F44C0C7D98D (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___matrix0, const RuntimeMethod* method) 
{
	{
		// return new float3x3(
		//     matrix.m00, matrix.m01, matrix.m02,
		//     matrix.m10, matrix.m11, matrix.m12,
		//     matrix.m20, matrix.m21, matrix.m22);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0 = ___matrix0;
		float L_1 = L_0.___m00_0;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_2 = ___matrix0;
		float L_3 = L_2.___m01_4;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_4 = ___matrix0;
		float L_5 = L_4.___m02_8;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_6 = ___matrix0;
		float L_7 = L_6.___m10_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_8 = ___matrix0;
		float L_9 = L_8.___m11_5;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_10 = ___matrix0;
		float L_11 = L_10.___m12_9;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_12 = ___matrix0;
		float L_13 = L_12.___m20_2;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_14 = ___matrix0;
		float L_15 = L_14.___m21_6;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_16 = ___matrix0;
		float L_17 = L_16.___m22_10;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_18;
		memset((&L_18), 0, sizeof(L_18));
		float3x3__ctor_m3AB31C9B587ABDCF15C8BF0E3A5B0158996A75ED_inline((&L_18), L_1, L_3, L_5, L_7, L_9, L_11, L_13, L_15, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Void CesiumForUnity.Helpers::MatrixToRotationAndScale(Unity.Mathematics.double3x3,Unity.Mathematics.quaternion&,Unity.Mathematics.double3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_MatrixToRotationAndScale_mA7BA8F572035E772AB8B038C797831E630BDAC46 (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___matrix0, quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4* ___rotation1, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___scale2, const RuntimeMethod* method) 
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// double lengthColumn0 = math.length(matrix.c0);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_0 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1 = L_0.___c0_0;
		double L_2;
		L_2 = math_length_m936CF76FF0C94E358B2193CFB59E41080B87E641_inline(L_1, NULL);
		V_0 = L_2;
		// double lengthColumn1 = math.length(matrix.c1);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_3 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = L_3.___c1_1;
		double L_5;
		L_5 = math_length_m936CF76FF0C94E358B2193CFB59E41080B87E641_inline(L_4, NULL);
		V_1 = L_5;
		// double lengthColumn2 = math.length(matrix.c2);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_6 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_7 = L_6.___c2_2;
		double L_8;
		L_8 = math_length_m936CF76FF0C94E358B2193CFB59E41080B87E641_inline(L_7, NULL);
		V_2 = L_8;
		// float3x3 rotationMatrix = new float3x3(
		//     (float3)(matrix.c0 / lengthColumn0),
		//     (float3)(matrix.c1 / lengthColumn1),
		//     (float3)(matrix.c2 / lengthColumn2));
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_9 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_10 = L_9.___c0_0;
		double L_11 = V_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_12;
		L_12 = double3_op_Division_mBFCCDD798F735189AE8D843BD014FCF5F1EEAD93_inline(L_10, L_11, NULL);
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_13;
		L_13 = float3_op_Explicit_mC39F75EB64FD16249FAD573FD8B6ADB14F132D78_inline(L_12, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_14 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_15 = L_14.___c1_1;
		double L_16 = V_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_17;
		L_17 = double3_op_Division_mBFCCDD798F735189AE8D843BD014FCF5F1EEAD93_inline(L_15, L_16, NULL);
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_18;
		L_18 = float3_op_Explicit_mC39F75EB64FD16249FAD573FD8B6ADB14F132D78_inline(L_17, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_19 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_20 = L_19.___c2_2;
		double L_21 = V_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_22;
		L_22 = double3_op_Division_mBFCCDD798F735189AE8D843BD014FCF5F1EEAD93_inline(L_20, L_21, NULL);
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_23;
		L_23 = float3_op_Explicit_mC39F75EB64FD16249FAD573FD8B6ADB14F132D78_inline(L_22, NULL);
		float3x3__ctor_mA652DC011B892B36A8216646B51B2014F89CE93E_inline((&V_3), L_13, L_18, L_23, NULL);
		// scale = new double3(lengthColumn0, lengthColumn1, lengthColumn2);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* L_24 = ___scale2;
		double L_25 = V_0;
		double L_26 = V_1;
		double L_27 = V_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_28;
		memset((&L_28), 0, sizeof(L_28));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_28), L_25, L_26, L_27, /*hidden argument*/NULL);
		*(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)L_24 = L_28;
		// double3 cross = math.cross(matrix.c0, matrix.c1);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_29 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_30 = L_29.___c0_0;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_31 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_32 = L_31.___c1_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_33;
		L_33 = math_cross_mD4DDFE34A1DA411148681014E59AEDC0655C0973_inline(L_30, L_32, NULL);
		// if (math.dot(cross, matrix.c2) < 0.0)
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_34 = ___matrix0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_35 = L_34.___c2_2;
		double L_36;
		L_36 = math_dot_m710CE5F525FC4891265B265568DE10C0100B509B_inline(L_33, L_35, NULL);
		if ((!(((double)L_36) < ((double)(0.0)))))
		{
			goto IL_00b9;
		}
	}
	{
		// rotationMatrix *= -1.0f;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_37 = V_3;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_38;
		L_38 = float3x3_op_Multiply_mF3B9F7F790D87EFB7EBC38F26ABDC9305816484A_inline(L_37, (-1.0f), NULL);
		V_3 = L_38;
		// scale *= -1.0f;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* L_39 = ___scale2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* L_40 = ___scale2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_41 = (*(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)L_40);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_42;
		L_42 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_41, (-1.0), NULL);
		*(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)L_39 = L_42;
	}

IL_00b9:
	{
		// rotation = math.quaternion(rotationMatrix);
		quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4* L_43 = ___rotation1;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_44 = V_3;
		quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 L_45;
		L_45 = math_quaternion_mE9DBDC1E38A93968B447FF4D365823A7889B0749_inline(L_44, NULL);
		*(quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4*)L_43 = L_45;
		// }
		return;
	}
}
// System.Void CesiumForUnity.Helpers::MatrixToTranslationRotationAndScale(Unity.Mathematics.double4x4,Unity.Mathematics.double3&,Unity.Mathematics.quaternion&,Unity.Mathematics.double3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_MatrixToTranslationRotationAndScale_m75BA42D7D45EABCAEBD7A5489BEFDE8908D352CF (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C ___matrix0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___translation1, quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4* ___rotation2, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* ___scale3, const RuntimeMethod* method) 
{
	{
		// translation = matrix.c3.xyz;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* L_0 = ___translation1;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_1 = (&(&___matrix0)->___c3_3);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2;
		L_2 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_1, NULL);
		*(double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4*)L_0 = L_2;
		// Helpers.MatrixToRotationAndScale(
		//     new double3x3(matrix.c0.xyz, matrix.c1.xyz, matrix.c2.xyz),
		//     out rotation,
		//     out scale);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_3 = (&(&___matrix0)->___c0_0);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4;
		L_4 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_3, NULL);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_5 = (&(&___matrix0)->___c1_1);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6;
		L_6 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_5, NULL);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_7 = (&(&___matrix0)->___c2_2);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8;
		L_8 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_7, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_9;
		memset((&L_9), 0, sizeof(L_9));
		double3x3__ctor_m0BF27C1E4D2C1C4965521A8B3A919CF9DB11B305_inline((&L_9), L_4, L_6, L_8, /*hidden argument*/NULL);
		quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4* L_10 = ___rotation2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* L_11 = ___scale3;
		Helpers_MatrixToRotationAndScale_mA7BA8F572035E772AB8B038C797831E630BDAC46(L_9, L_10, L_11, NULL);
		// }
		return;
	}
}
// System.Void CesiumForUnity.Helpers::MatrixToInaccurateRotationAndScale(Unity.Mathematics.double3x3,UnityEngine.Quaternion&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_MatrixToInaccurateRotationAndScale_m47E8DEAED109886AABCC62F11B18860885F58EC6 (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___matrix0, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* ___rotation1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___scale2, const RuntimeMethod* method) 
{
	quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// MatrixToRotationAndScale(matrix, out rotationTemp, out scaleTemp);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_0 = ___matrix0;
		Helpers_MatrixToRotationAndScale_mA7BA8F572035E772AB8B038C797831E630BDAC46(L_0, (&V_0), (&V_1), NULL);
		// rotation = rotationTemp;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* L_1 = ___rotation1;
		quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 L_2 = V_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_3;
		L_3 = quaternion_op_Implicit_m78F07E28F5AB9C26F0EE997B2F12AF4A209FCD41(L_2, NULL);
		*(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974*)L_1 = L_3;
		// scale = (float3)scaleTemp;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_4 = ___scale2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5 = V_1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_6;
		L_6 = float3_op_Explicit_mC39F75EB64FD16249FAD573FD8B6ADB14F132D78_inline(L_5, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = float3_op_Implicit_m9CC301DFD67EEFAA15CA05E91913E862B22326F6(L_6, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_4 = L_7;
		// }
		return;
	}
}
// System.Void CesiumForUnity.Helpers::MatrixToInaccurateTranslationRotationAndScale(Unity.Mathematics.double4x4,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers_MatrixToInaccurateTranslationRotationAndScale_m7BBC31EED0356F7C67B2D1CC3C179E980110DA3E (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C ___matrix0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___translation1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* ___rotation2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___scale3, const RuntimeMethod* method) 
{
	{
		// translation = Helpers.FromMathematics(matrix.c3.xyz);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_0 = ___translation1;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_1 = (&(&___matrix0)->___c3_3);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2;
		L_2 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_1, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Helpers_FromMathematics_m8CB96E41C8AFC98D3FBDBD51283C7A77D4655409(L_2, NULL);
		*(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*)L_0 = L_3;
		// Helpers.MatrixToInaccurateRotationAndScale(
		//     new double3x3(matrix.c0.xyz, matrix.c1.xyz, matrix.c2.xyz),
		//     out rotation,
		//     out scale);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_4 = (&(&___matrix0)->___c0_0);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5;
		L_5 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_4, NULL);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_6 = (&(&___matrix0)->___c1_1);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_7;
		L_7 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_6, NULL);
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* L_8 = (&(&___matrix0)->___c2_2);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_9;
		L_9 = double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline(L_8, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_10;
		memset((&L_10), 0, sizeof(L_10));
		double3x3__ctor_m0BF27C1E4D2C1C4965521A8B3A919CF9DB11B305_inline((&L_10), L_5, L_7, L_9, /*hidden argument*/NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* L_11 = ___rotation2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_12 = ___scale3;
		Helpers_MatrixToInaccurateRotationAndScale_m47E8DEAED109886AABCC62F11B18860885F58EC6(L_10, L_11, L_12, NULL);
		// }
		return;
	}
}
// Unity.Mathematics.double4x4 CesiumForUnity.Helpers::TranslationRotationAndScaleToMatrix(Unity.Mathematics.double3,Unity.Mathematics.quaternion,Unity.Mathematics.double3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C Helpers_TranslationRotationAndScaleToMatrix_m16B1C0AC46551F519EE951D24DEAD7B3C225EE50 (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___translation0, quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 ___rotation1, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___scale2, const RuntimeMethod* method) 
{
	double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 V_0;
	memset((&V_0), 0, sizeof(V_0));
	double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// double3x3 scaleMatrix = new double3x3(
		//     new double3(scale.x, 0.0, 0.0),
		//     new double3(0.0, scale.y, 0.0),
		//     new double3(0.0, 0.0, scale.z));
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___scale2;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2;
		memset((&L_2), 0, sizeof(L_2));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_2), L_1, (0.0), (0.0), /*hidden argument*/NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3 = ___scale2;
		double L_4 = L_3.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5;
		memset((&L_5), 0, sizeof(L_5));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_5), (0.0), L_4, (0.0), /*hidden argument*/NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___scale2;
		double L_7 = L_6.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8;
		memset((&L_8), 0, sizeof(L_8));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_8), (0.0), (0.0), L_7, /*hidden argument*/NULL);
		double3x3__ctor_m0BF27C1E4D2C1C4965521A8B3A919CF9DB11B305_inline((&V_0), L_2, L_5, L_8, NULL);
		// double3x3 rotationMatrix = new float3x3(rotation);
		quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 L_9 = ___rotation1;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_10;
		memset((&L_10), 0, sizeof(L_10));
		float3x3__ctor_mF94488DFF7867CFC89648E024FA89A19F23E2FAE((&L_10), L_9, /*hidden argument*/NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_11;
		L_11 = double3x3_op_Implicit_mDE9DBCF7F737C1128250D072AF94867314B7FAA1_inline(L_10, NULL);
		// double3x3 scaleAndRotate = math.mul(rotationMatrix, scaleMatrix);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_12 = V_0;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_13;
		L_13 = math_mul_m8006A1F722590AD2791FB2B506A1A74A0816494F_inline(L_11, L_12, NULL);
		V_1 = L_13;
		// return new double4x4(
		//     new double4(scaleAndRotate.c0, 0.0),
		//     new double4(scaleAndRotate.c1, 0.0),
		//     new double4(scaleAndRotate.c2, 0.0),
		//     new double4(translation, 1.0));
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_14 = V_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_15 = L_14.___c0_0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_16;
		memset((&L_16), 0, sizeof(L_16));
		double4__ctor_mAAB30D5A18E63BBEB9AD9B98E95D510784E64B26_inline((&L_16), L_15, (0.0), /*hidden argument*/NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_17 = V_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_18 = L_17.___c1_1;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_19;
		memset((&L_19), 0, sizeof(L_19));
		double4__ctor_mAAB30D5A18E63BBEB9AD9B98E95D510784E64B26_inline((&L_19), L_18, (0.0), /*hidden argument*/NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_20 = V_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_21 = L_20.___c2_2;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_22;
		memset((&L_22), 0, sizeof(L_22));
		double4__ctor_mAAB30D5A18E63BBEB9AD9B98E95D510784E64B26_inline((&L_22), L_21, (0.0), /*hidden argument*/NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_23 = ___translation0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_24;
		memset((&L_24), 0, sizeof(L_24));
		double4__ctor_mAAB30D5A18E63BBEB9AD9B98E95D510784E64B26_inline((&L_24), L_23, (1.0), /*hidden argument*/NULL);
		double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C L_25;
		memset((&L_25), 0, sizeof(L_25));
		double4x4__ctor_mC28DA9877244770A1E61E41A50DF95F405AFD34B_inline((&L_25), L_16, L_19, L_22, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Void CesiumForUnity.Helpers::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Helpers__ctor_m22DFC8AF24CE9030865949917647F23F28DE5D43 (Helpers_tF67CB24E1B3D10F6E551B7D1DC5ED497AC3E1DD2* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.NativeCoroutine::.ctor(System.Func`2<System.Object,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeCoroutine__ctor_mBE7299BB32AE69B10EF1F1EF12B6DD132CC4CCCF (NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* __this, Func_2_tACBF5A1656250800CE861707354491F0611F6624* ___callback0, const RuntimeMethod* method) 
{
	{
		// public NativeCoroutine(Func<object, object> callback)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// _callback = callback;
		Func_2_tACBF5A1656250800CE861707354491F0611F6624* L_0 = ___callback0;
		__this->____callback_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____callback_0), (void*)L_0);
		// }
		return;
	}
}
// System.Collections.IEnumerator CesiumForUnity.NativeCoroutine::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeCoroutine_GetEnumerator_mA9822DAE36EB6BBB6847643335C2B1277C7FE47D (NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* L_0 = (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1*)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CGetEnumeratorU3Ed__2__ctor_m33BA9725115B0FB505A76B96DF2E1D9827F32492(L_0, 0, NULL);
		U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetEnumeratorU3Ed__2__ctor_m33BA9725115B0FB505A76B96DF2E1D9827F32492 (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_mA4D8FBD867DF73ED5662B1A48C95A0C6D44D2E23 (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CGetEnumeratorU3Ed__2_MoveNext_m3C91BF9AD8A40BFB7684936B2817712917402DF9 (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* L_1 = __this->___U3CU3E4__this_2;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// object sentinel = this._callback;
		NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* L_4 = V_1;
		NullCheck(L_4);
		Func_2_tACBF5A1656250800CE861707354491F0611F6624* L_5 = L_4->____callback_0;
		__this->___U3CsentinelU3E5__2_3 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CsentinelU3E5__2_3), (void*)L_5);
		// object next = this._callback(sentinel);
		NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* L_6 = V_1;
		NullCheck(L_6);
		Func_2_tACBF5A1656250800CE861707354491F0611F6624* L_7 = L_6->____callback_0;
		RuntimeObject* L_8 = __this->___U3CsentinelU3E5__2_3;
		NullCheck(L_7);
		RuntimeObject* L_9;
		L_9 = Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_inline(L_7, L_8, NULL);
		V_2 = L_9;
		goto IL_0067;
	}

IL_003e:
	{
		// yield return next;
		RuntimeObject* L_10 = V_2;
		__this->___U3CU3E2__current_1 = L_10;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_10);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_004e:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// next = this._callback(sentinel);
		NativeCoroutine_tF6B751502085E2D89100E3ACC4597DF6F792DC34* L_11 = V_1;
		NullCheck(L_11);
		Func_2_tACBF5A1656250800CE861707354491F0611F6624* L_12 = L_11->____callback_0;
		RuntimeObject* L_13 = __this->___U3CsentinelU3E5__2_3;
		NullCheck(L_12);
		RuntimeObject* L_14;
		L_14 = Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_inline(L_12, L_13, NULL);
		V_2 = L_14;
	}

IL_0067:
	{
		// while (next != sentinel)
		RuntimeObject* L_15 = V_2;
		RuntimeObject* L_16 = __this->___U3CsentinelU3E5__2_3;
		if ((!(((RuntimeObject*)(RuntimeObject*)L_15) == ((RuntimeObject*)(RuntimeObject*)L_16))))
		{
			goto IL_003e;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79E3EC97A8C06FF6AE7658DF0D312D44DA8AD6AA (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE8E016962F281593E81FBD2CD898B99EC0D7E49F (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE8E016962F281593E81FBD2CD898B99EC0D7E49F_RuntimeMethod_var)));
	}
}
// System.Object CesiumForUnity.NativeCoroutine/<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_mAFD1F581B3125BCFB1F33C43240DD11A27F36C0B (U3CGetEnumeratorU3Ed__2_t5DF868267473826A8EB41B24EB481BB92CBC82A1* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CesiumForUnity.UnityLifetime::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityLifetime_Destroy_m1312A10229B3838AB3A5EBE00C70E55733D94FA9 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___o0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Object.Destroy(o);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_0 = ___o0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mE97D0A766419A81296E8D4E5C23D01D3FE91ACBB(L_0, NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void SafeHandle_SetHandle_m003D64748F9DFBA1E3C0B23798C23BA81AA21C2A_inline (SafeHandle_tC1A4DA80DA89B867CC011B707A07275230321BF7* __this, intptr_t ___handle0, const RuntimeMethod* method) 
{
	{
		intptr_t L_0 = ___handle0;
		__this->___handle_0 = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_cmax_mD1CA685960C6D3E73AE61E158449D1F136B2D8D9_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, const RuntimeMethod* method) 
{
	{
		// public static double cmax(double3 x) { return max(max(x.x, x.y), x.z); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___x0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___x0;
		double L_3 = L_2.___y_1;
		double L_4;
		L_4 = math_max_m8830F8721EFC73BCF991CD497115A103B86BF3BE_inline(L_1, L_3, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5 = ___x0;
		double L_6 = L_5.___z_2;
		double L_7;
		L_7 = math_max_m8830F8721EFC73BCF991CD497115A103B86BF3BE_inline(L_4, L_6, NULL);
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_cmin_mD62CF2BF7B13402E46E966F3BED814004E5D8C65_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, const RuntimeMethod* method) 
{
	{
		// public static double cmin(double3 x) { return min(min(x.x, x.y), x.z); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___x0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___x0;
		double L_3 = L_2.___y_1;
		double L_4;
		L_4 = math_min_m29A6A5FB36524D911D13DDB4866FF005C7BF00D5_inline(L_1, L_3, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5 = ___x0;
		double L_6 = L_5.___z_2;
		double L_7;
		L_7 = math_min_m29A6A5FB36524D911D13DDB4866FF005C7BF00D5_inline(L_4, L_6, NULL);
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_1 = L_0;
		float L_1 = ___y1;
		__this->___y_2 = L_1;
		float L_2 = ___z2;
		__this->___z_3 = L_2;
		float L_3 = ___w3;
		__this->___w_4 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4x4__ctor_mDB1C9BED251AFC0CD16CA1D52545C5A1DAA6878F_inline (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C* __this, double ___m000, double ___m011, double ___m022, double ___m033, double ___m104, double ___m115, double ___m126, double ___m137, double ___m208, double ___m219, double ___m2210, double ___m2311, double ___m3012, double ___m3113, double ___m3214, double ___m3315, const RuntimeMethod* method) 
{
	{
		// this.c0 = new double4(m00, m10, m20, m30);
		double L_0 = ___m000;
		double L_1 = ___m104;
		double L_2 = ___m208;
		double L_3 = ___m3012;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_4;
		memset((&L_4), 0, sizeof(L_4));
		double4__ctor_m49D96B66F7E9E5F0783AA40FCBE7EC199F5C7C42_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->___c0_0 = L_4;
		// this.c1 = new double4(m01, m11, m21, m31);
		double L_5 = ___m011;
		double L_6 = ___m115;
		double L_7 = ___m219;
		double L_8 = ___m3113;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_9;
		memset((&L_9), 0, sizeof(L_9));
		double4__ctor_m49D96B66F7E9E5F0783AA40FCBE7EC199F5C7C42_inline((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->___c1_1 = L_9;
		// this.c2 = new double4(m02, m12, m22, m32);
		double L_10 = ___m022;
		double L_11 = ___m126;
		double L_12 = ___m2210;
		double L_13 = ___m3214;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_14;
		memset((&L_14), 0, sizeof(L_14));
		double4__ctor_m49D96B66F7E9E5F0783AA40FCBE7EC199F5C7C42_inline((&L_14), L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->___c2_2 = L_14;
		// this.c3 = new double4(m03, m13, m23, m33);
		double L_15 = ___m033;
		double L_16 = ___m137;
		double L_17 = ___m2311;
		double L_18 = ___m3315;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_19;
		memset((&L_19), 0, sizeof(L_19));
		double4__ctor_m49D96B66F7E9E5F0783AA40FCBE7EC199F5C7C42_inline((&L_19), L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		__this->___c3_3 = L_19;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3x3__ctor_mBEE4C5D1CCF08BD6C8E94DD819F144FBC690E888_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0* __this, double ___m000, double ___m011, double ___m022, double ___m103, double ___m114, double ___m125, double ___m206, double ___m217, double ___m228, const RuntimeMethod* method) 
{
	{
		// this.c0 = new double3(m00, m10, m20);
		double L_0 = ___m000;
		double L_1 = ___m103;
		double L_2 = ___m206;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3;
		memset((&L_3), 0, sizeof(L_3));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->___c0_0 = L_3;
		// this.c1 = new double3(m01, m11, m21);
		double L_4 = ___m011;
		double L_5 = ___m114;
		double L_6 = ___m217;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_7;
		memset((&L_7), 0, sizeof(L_7));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_7), L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->___c1_1 = L_7;
		// this.c2 = new double3(m02, m12, m22);
		double L_8 = ___m022;
		double L_9 = ___m125;
		double L_10 = ___m228;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_11;
		memset((&L_11), 0, sizeof(L_11));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_11), L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->___c2_2 = L_11;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_m3AB31C9B587ABDCF15C8BF0E3A5B0158996A75ED_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79* __this, float ___m000, float ___m011, float ___m022, float ___m103, float ___m114, float ___m125, float ___m206, float ___m217, float ___m228, const RuntimeMethod* method) 
{
	{
		// this.c0 = new float3(m00, m10, m20);
		float L_0 = ___m000;
		float L_1 = ___m103;
		float L_2 = ___m206;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mC61002CD0EC13D7C37D846D021A78C028FB80DB9_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->___c0_0 = L_3;
		// this.c1 = new float3(m01, m11, m21);
		float L_4 = ___m011;
		float L_5 = ___m114;
		float L_6 = ___m217;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_7;
		memset((&L_7), 0, sizeof(L_7));
		float3__ctor_mC61002CD0EC13D7C37D846D021A78C028FB80DB9_inline((&L_7), L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->___c1_1 = L_7;
		// this.c2 = new float3(m02, m12, m22);
		float L_8 = ___m022;
		float L_9 = ___m125;
		float L_10 = ___m228;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_11;
		memset((&L_11), 0, sizeof(L_11));
		float3__ctor_mC61002CD0EC13D7C37D846D021A78C028FB80DB9_inline((&L_11), L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->___c2_2 = L_11;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_length_m936CF76FF0C94E358B2193CFB59E41080B87E641_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, const RuntimeMethod* method) 
{
	{
		// public static double length(double3 x) { return sqrt(dot(x, x)); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___x0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1 = ___x0;
		double L_2;
		L_2 = math_dot_m710CE5F525FC4891265B265568DE10C0100B509B_inline(L_0, L_1, NULL);
		double L_3;
		L_3 = math_sqrt_mA3A9D5DFDF6841F8836E3ECD5D83555842383F36_inline(L_2, NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Division_mBFCCDD798F735189AE8D843BD014FCF5F1EEAD93_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static double3 operator / (double3 lhs, double rhs) { return new double3 (lhs.x / rhs, lhs.y / rhs, lhs.z / rhs); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___lhs0;
		double L_1 = L_0.___x_0;
		double L_2 = ___rhs1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3 = ___lhs0;
		double L_4 = L_3.___y_1;
		double L_5 = ___rhs1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___lhs0;
		double L_7 = L_6.___z_2;
		double L_8 = ___rhs1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_9;
		memset((&L_9), 0, sizeof(L_9));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_9), ((double)(L_1/L_2)), ((double)(L_4/L_5)), ((double)(L_7/L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E float3_op_Explicit_mC39F75EB64FD16249FAD573FD8B6ADB14F132D78_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___v0, const RuntimeMethod* method) 
{
	{
		// public static explicit operator float3(double3 v) { return new float3(v); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___v0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_1;
		memset((&L_1), 0, sizeof(L_1));
		float3__ctor_mD7BFFAB3D7057D71DB7B2F5A50788D197E1AA49B_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_mA652DC011B892B36A8216646B51B2014F89CE93E_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79* __this, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c00, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c11, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___c22, const RuntimeMethod* method) 
{
	{
		// this.c0 = c0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_0 = ___c00;
		__this->___c0_0 = L_0;
		// this.c1 = c1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_1 = ___c11;
		__this->___c1_1 = L_1;
		// this.c2 = c2;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_2 = ___c22;
		__this->___c2_2 = L_2;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* __this, double ___x0, double ___y1, double ___z2, const RuntimeMethod* method) 
{
	{
		// this.x = x;
		double L_0 = ___x0;
		__this->___x_0 = L_0;
		// this.y = y;
		double L_1 = ___y1;
		__this->___y_1 = L_1;
		// this.z = z;
		double L_2 = ___z2;
		__this->___z_2 = L_2;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 math_cross_mD4DDFE34A1DA411148681014E59AEDC0655C0973_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___y1, const RuntimeMethod* method) 
{
	double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// public static double3 cross(double3 x, double3 y) { return (x * y.yzx - x.yzx * y).yzx; }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___x0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1;
		L_1 = double3_get_yzx_mFEFD36EE9E6E6470EDDCF595DEAAB85FCBAC2795_inline((&___y1), NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2;
		L_2 = double3_op_Multiply_mFF3B33CAB54AB767C1B7927B97658C307150BCA9_inline(L_0, L_1, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3;
		L_3 = double3_get_yzx_mFEFD36EE9E6E6470EDDCF595DEAAB85FCBAC2795_inline((&___x0), NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___y1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5;
		L_5 = double3_op_Multiply_mFF3B33CAB54AB767C1B7927B97658C307150BCA9_inline(L_3, L_4, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6;
		L_6 = double3_op_Subtraction_m22E94C140DA02DCD57ADB54B6DEEFA271AEB82A0_inline(L_2, L_5, NULL);
		V_0 = L_6;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_7;
		L_7 = double3_get_yzx_mFEFD36EE9E6E6470EDDCF595DEAAB85FCBAC2795_inline((&V_0), NULL);
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_dot_m710CE5F525FC4891265B265568DE10C0100B509B_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___x0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___y1, const RuntimeMethod* method) 
{
	{
		// public static double dot(double3 x, double3 y) { return x.x * y.x + x.y * y.y + x.z * y.z; }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___x0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___y1;
		double L_3 = L_2.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___x0;
		double L_5 = L_4.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___y1;
		double L_7 = L_6.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8 = ___x0;
		double L_9 = L_8.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_10 = ___y1;
		double L_11 = L_10.___z_2;
		return ((double)il2cpp_codegen_add(((double)il2cpp_codegen_add(((double)il2cpp_codegen_multiply(L_1, L_3)), ((double)il2cpp_codegen_multiply(L_5, L_7)))), ((double)il2cpp_codegen_multiply(L_9, L_11))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 float3x3_op_Multiply_mF3B9F7F790D87EFB7EBC38F26ABDC9305816484A_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___lhs0, float ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static float3x3 operator * (float3x3 lhs, float rhs) { return new float3x3 (lhs.c0 * rhs, lhs.c1 * rhs, lhs.c2 * rhs); }
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_0 = ___lhs0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_1 = L_0.___c0_0;
		float L_2 = ___rhs1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_3;
		L_3 = float3_op_Multiply_m6E5DC552C8B0F9A180298BD9197FF47B14E0EA81_inline(L_1, L_2, NULL);
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_4 = ___lhs0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_5 = L_4.___c1_1;
		float L_6 = ___rhs1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_7;
		L_7 = float3_op_Multiply_m6E5DC552C8B0F9A180298BD9197FF47B14E0EA81_inline(L_5, L_6, NULL);
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_8 = ___lhs0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_9 = L_8.___c2_2;
		float L_10 = ___rhs1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_11;
		L_11 = float3_op_Multiply_m6E5DC552C8B0F9A180298BD9197FF47B14E0EA81_inline(L_9, L_10, NULL);
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3x3__ctor_mA652DC011B892B36A8216646B51B2014F89CE93E_inline((&L_12), L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static double3 operator * (double3 lhs, double rhs) { return new double3 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___lhs0;
		double L_1 = L_0.___x_0;
		double L_2 = ___rhs1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3 = ___lhs0;
		double L_4 = L_3.___y_1;
		double L_5 = ___rhs1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___lhs0;
		double L_7 = L_6.___z_2;
		double L_8 = ___rhs1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_9;
		memset((&L_9), 0, sizeof(L_9));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_9), ((double)il2cpp_codegen_multiply(L_1, L_2)), ((double)il2cpp_codegen_multiply(L_4, L_5)), ((double)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 math_quaternion_mE9DBDC1E38A93968B447FF4D365823A7889B0749_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___m0, const RuntimeMethod* method) 
{
	{
		// public static quaternion quaternion(float3x3 m) { return new quaternion(m); }
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_0 = ___m0;
		quaternion_tD6BCBECAF088B9EBAE2345EC8534C7A1A4C910D4 L_1;
		memset((&L_1), 0, sizeof(L_1));
		quaternion__ctor_m354F09C0E50CA59DA43037E9993EAE9BF97E9120((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double4_get_xyz_m1535A1EC6086B24AB7C384EF03935A4133194425_inline (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* __this, const RuntimeMethod* method) 
{
	{
		// get { return new double3(x, y, z); }
		double L_0 = __this->___x_0;
		double L_1 = __this->___y_1;
		double L_2 = __this->___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3;
		memset((&L_3), 0, sizeof(L_3));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3x3__ctor_m0BF27C1E4D2C1C4965521A8B3A919CF9DB11B305_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c00, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c11, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c22, const RuntimeMethod* method) 
{
	{
		// this.c0 = c0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___c00;
		__this->___c0_0 = L_0;
		// this.c1 = c1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1 = ___c11;
		__this->___c1_1 = L_1;
		// this.c2 = c2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___c22;
		__this->___c2_2 = L_2;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 double3x3_op_Implicit_mDE9DBCF7F737C1128250D072AF94867314B7FAA1_inline (float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___v0, const RuntimeMethod* method) 
{
	{
		// public static implicit operator double3x3(float3x3 v) { return new double3x3(v); }
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_0 = ___v0;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_1;
		memset((&L_1), 0, sizeof(L_1));
		double3x3__ctor_m4A89254CD6C32BCF5BCEBC60A4E712E2360DD972_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 math_mul_m8006A1F722590AD2791FB2B506A1A74A0816494F_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___a0, double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 ___b1, const RuntimeMethod* method) 
{
	{
		// return double3x3(
		//     a.c0 * b.c0.x + a.c1 * b.c0.y + a.c2 * b.c0.z,
		//     a.c0 * b.c1.x + a.c1 * b.c1.y + a.c2 * b.c1.z,
		//     a.c0 * b.c2.x + a.c1 * b.c2.y + a.c2 * b.c2.z);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_0 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1 = L_0.___c0_0;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_2 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3 = L_2.___c0_0;
		double L_4 = L_3.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5;
		L_5 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_1, L_4, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_6 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_7 = L_6.___c1_1;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_8 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_9 = L_8.___c0_0;
		double L_10 = L_9.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_11;
		L_11 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_7, L_10, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_12;
		L_12 = double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline(L_5, L_11, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_13 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_14 = L_13.___c2_2;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_15 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_16 = L_15.___c0_0;
		double L_17 = L_16.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_18;
		L_18 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_14, L_17, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_19;
		L_19 = double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline(L_12, L_18, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_20 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_21 = L_20.___c0_0;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_22 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_23 = L_22.___c1_1;
		double L_24 = L_23.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_25;
		L_25 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_21, L_24, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_26 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_27 = L_26.___c1_1;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_28 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_29 = L_28.___c1_1;
		double L_30 = L_29.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_31;
		L_31 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_27, L_30, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_32;
		L_32 = double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline(L_25, L_31, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_33 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_34 = L_33.___c2_2;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_35 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_36 = L_35.___c1_1;
		double L_37 = L_36.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_38;
		L_38 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_34, L_37, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_39;
		L_39 = double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline(L_32, L_38, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_40 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_41 = L_40.___c0_0;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_42 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_43 = L_42.___c2_2;
		double L_44 = L_43.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_45;
		L_45 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_41, L_44, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_46 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_47 = L_46.___c1_1;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_48 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_49 = L_48.___c2_2;
		double L_50 = L_49.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_51;
		L_51 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_47, L_50, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_52;
		L_52 = double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline(L_45, L_51, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_53 = ___a0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_54 = L_53.___c2_2;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_55 = ___b1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_56 = L_55.___c2_2;
		double L_57 = L_56.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_58;
		L_58 = double3_op_Multiply_mF18D6011FB9D647C1F1A430FA272B91736A07AC8_inline(L_54, L_57, NULL);
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_59;
		L_59 = double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline(L_52, L_58, NULL);
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_60;
		L_60 = math_double3x3_mDEDFD3D5E0FAD4EC0550DE55A86A7D199B3CC61B_inline(L_19, L_39, L_59, NULL);
		return L_60;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4__ctor_mAAB30D5A18E63BBEB9AD9B98E95D510784E64B26_inline (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___xyz0, double ___w1, const RuntimeMethod* method) 
{
	{
		// this.x = xyz.x;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___xyz0;
		double L_1 = L_0.___x_0;
		__this->___x_0 = L_1;
		// this.y = xyz.y;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___xyz0;
		double L_3 = L_2.___y_1;
		__this->___y_1 = L_3;
		// this.z = xyz.z;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___xyz0;
		double L_5 = L_4.___z_2;
		__this->___z_2 = L_5;
		// this.w = w;
		double L_6 = ___w1;
		__this->___w_3 = L_6;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4x4__ctor_mC28DA9877244770A1E61E41A50DF95F405AFD34B_inline (double4x4_tB452F9489714C6B8D74D46CA2CF1F0CA8F185D3C* __this, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c00, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c11, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c22, double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 ___c33, const RuntimeMethod* method) 
{
	{
		// this.c0 = c0;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_0 = ___c00;
		__this->___c0_0 = L_0;
		// this.c1 = c1;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_1 = ___c11;
		__this->___c1_1 = L_1;
		// this.c2 = c2;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_2 = ___c22;
		__this->___c2_2 = L_2;
		// this.c3 = c3;
		double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5 L_3 = ___c33;
		__this->___c3_3 = L_3;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Func_2_Invoke_mDBA25DA5DA5B7E056FB9B026AF041F1385FB58A9_gshared_inline (Func_2_tACBF5A1656250800CE861707354491F0611F6624* __this, RuntimeObject* ___arg0, const RuntimeMethod* method) 
{
	typedef RuntimeObject* (*FunctionPointerType) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*);
	return ((FunctionPointerType)__this->___invoke_impl_1)((Il2CppObject*)__this->___method_code_6, ___arg0, reinterpret_cast<RuntimeMethod*>(__this->___method_3));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_max_m8830F8721EFC73BCF991CD497115A103B86BF3BE_inline (double ___x0, double ___y1, const RuntimeMethod* method) 
{
	{
		// public static double max(double x, double y) { return double.IsNaN(y) || x > y ? x : y; }
		double L_0 = ___y1;
		bool L_1;
		L_1 = Double_IsNaN_mF2BC6D1FD4813179B2CAE58D29770E42830D0883_inline(L_0, NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		double L_2 = ___x0;
		double L_3 = ___y1;
		if ((((double)L_2) > ((double)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		double L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		double L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_min_m29A6A5FB36524D911D13DDB4866FF005C7BF00D5_inline (double ___x0, double ___y1, const RuntimeMethod* method) 
{
	{
		// public static double min(double x, double y) { return double.IsNaN(y) || x < y ? x : y; }
		double L_0 = ___y1;
		bool L_1;
		L_1 = Double_IsNaN_mF2BC6D1FD4813179B2CAE58D29770E42830D0883_inline(L_0, NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		double L_2 = ___x0;
		double L_3 = ___y1;
		if ((((double)L_2) < ((double)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		double L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		double L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double4__ctor_m49D96B66F7E9E5F0783AA40FCBE7EC199F5C7C42_inline (double4_t82EF3F10905F7357C3F8C08F83AB6F8EC776FDC5* __this, double ___x0, double ___y1, double ___z2, double ___w3, const RuntimeMethod* method) 
{
	{
		// this.x = x;
		double L_0 = ___x0;
		__this->___x_0 = L_0;
		// this.y = y;
		double L_1 = ___y1;
		__this->___y_1 = L_1;
		// this.z = z;
		double L_2 = ___z2;
		__this->___z_2 = L_2;
		// this.w = w;
		double L_3 = ___w3;
		__this->___w_3 = L_3;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mC61002CD0EC13D7C37D846D021A78C028FB80DB9_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		// this.y = y;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		// this.z = z;
		float L_2 = ___z2;
		__this->___z_2 = L_2;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double math_sqrt_mA3A9D5DFDF6841F8836E3ECD5D83555842383F36_inline (double ___x0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static double sqrt(double x) { return System.Math.Sqrt(x); }
		double L_0 = ___x0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sqrt(L_0);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mD7BFFAB3D7057D71DB7B2F5A50788D197E1AA49B_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E* __this, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___v0, const RuntimeMethod* method) 
{
	{
		// this.x = (float)v.x;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___v0;
		double L_1 = L_0.___x_0;
		__this->___x_0 = ((float)L_1);
		// this.y = (float)v.y;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___v0;
		double L_3 = L_2.___y_1;
		__this->___y_1 = ((float)L_3);
		// this.z = (float)v.z;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___v0;
		double L_5 = L_4.___z_2;
		__this->___z_2 = ((float)L_5);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_get_yzx_mFEFD36EE9E6E6470EDDCF595DEAAB85FCBAC2795_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* __this, const RuntimeMethod* method) 
{
	{
		// get { return new double3(y, z, x); }
		double L_0 = __this->___y_1;
		double L_1 = __this->___z_2;
		double L_2 = __this->___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_3;
		memset((&L_3), 0, sizeof(L_3));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Multiply_mFF3B33CAB54AB767C1B7927B97658C307150BCA9_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static double3 operator * (double3 lhs, double3 rhs) { return new double3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___lhs0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___rhs1;
		double L_3 = L_2.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___lhs0;
		double L_5 = L_4.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___rhs1;
		double L_7 = L_6.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8 = ___lhs0;
		double L_9 = L_8.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_10 = ___rhs1;
		double L_11 = L_10.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_12;
		memset((&L_12), 0, sizeof(L_12));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_12), ((double)il2cpp_codegen_multiply(L_1, L_3)), ((double)il2cpp_codegen_multiply(L_5, L_7)), ((double)il2cpp_codegen_multiply(L_9, L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Subtraction_m22E94C140DA02DCD57ADB54B6DEEFA271AEB82A0_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static double3 operator - (double3 lhs, double3 rhs) { return new double3 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___lhs0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___rhs1;
		double L_3 = L_2.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___lhs0;
		double L_5 = L_4.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___rhs1;
		double L_7 = L_6.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8 = ___lhs0;
		double L_9 = L_8.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_10 = ___rhs1;
		double L_11 = L_10.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_12;
		memset((&L_12), 0, sizeof(L_12));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_12), ((double)il2cpp_codegen_subtract(L_1, L_3)), ((double)il2cpp_codegen_subtract(L_5, L_7)), ((double)il2cpp_codegen_subtract(L_9, L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E float3_op_Multiply_m6E5DC552C8B0F9A180298BD9197FF47B14E0EA81_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___lhs0, float ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static float3 operator * (float3 lhs, float rhs) { return new float3 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_0 = ___lhs0;
		float L_1 = L_0.___x_0;
		float L_2 = ___rhs1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_3 = ___lhs0;
		float L_4 = L_3.___y_1;
		float L_5 = ___rhs1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_6 = ___lhs0;
		float L_7 = L_6.___z_2;
		float L_8 = ___rhs1;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mC61002CD0EC13D7C37D846D021A78C028FB80DB9_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3x3__ctor_m4A89254CD6C32BCF5BCEBC60A4E712E2360DD972_inline (double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0* __this, float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 ___v0, const RuntimeMethod* method) 
{
	{
		// this.c0 = v.c0;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_0 = ___v0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_1 = L_0.___c0_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2;
		L_2 = double3_op_Implicit_m5DCE807570E8C929820AE8D221FFEE4861D9D5D9_inline(L_1, NULL);
		__this->___c0_0 = L_2;
		// this.c1 = v.c1;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_3 = ___v0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_4 = L_3.___c1_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_5;
		L_5 = double3_op_Implicit_m5DCE807570E8C929820AE8D221FFEE4861D9D5D9_inline(L_4, NULL);
		__this->___c1_1 = L_5;
		// this.c2 = v.c2;
		float3x3_tB318DB8C7E54B6CA9E14EB9AC7F5964C1189FC79 L_6 = ___v0;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_7 = L_6.___c2_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8;
		L_8 = double3_op_Implicit_m5DCE807570E8C929820AE8D221FFEE4861D9D5D9_inline(L_7, NULL);
		__this->___c2_2 = L_8;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Addition_mBAAE8EB7B08FA0F788CDC40FB633F4ACC0089DCA_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___lhs0, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___rhs1, const RuntimeMethod* method) 
{
	{
		// public static double3 operator + (double3 lhs, double3 rhs) { return new double3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___lhs0;
		double L_1 = L_0.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___rhs1;
		double L_3 = L_2.___x_0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_4 = ___lhs0;
		double L_5 = L_4.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_6 = ___rhs1;
		double L_7 = L_6.___y_1;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_8 = ___lhs0;
		double L_9 = L_8.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_10 = ___rhs1;
		double L_11 = L_10.___z_2;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_12;
		memset((&L_12), 0, sizeof(L_12));
		double3__ctor_mDF4F1B343383615E326E74EAE18FC4325F7367E1_inline((&L_12), ((double)il2cpp_codegen_add(L_1, L_3)), ((double)il2cpp_codegen_add(L_5, L_7)), ((double)il2cpp_codegen_add(L_9, L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 math_double3x3_mDEDFD3D5E0FAD4EC0550DE55A86A7D199B3CC61B_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c00, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c11, double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 ___c22, const RuntimeMethod* method) 
{
	{
		// public static double3x3 double3x3(double3 c0, double3 c1, double3 c2) { return new double3x3(c0, c1, c2); }
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_0 = ___c00;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1 = ___c11;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_2 = ___c22;
		double3x3_t3FC5A5668AD878A74586326751AF9BE7B1CEBBB0 L_3;
		memset((&L_3), 0, sizeof(L_3));
		double3x3__ctor_m0BF27C1E4D2C1C4965521A8B3A919CF9DB11B305_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Double_IsNaN_mF2BC6D1FD4813179B2CAE58D29770E42830D0883_inline (double ___d0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		double L_0 = ___d0;
		il2cpp_codegen_runtime_class_init_inline(BitConverter_t6E99605185963BC12B3D369E13F2B88997E64A27_il2cpp_TypeInfo_var);
		int64_t L_1;
		L_1 = BitConverter_DoubleToInt64Bits_m4F42741818550F9956B5FBAF88C051F4DE5B0AE6_inline(L_0, NULL);
		return (bool)((((int64_t)((int64_t)(L_1&((int64_t)(std::numeric_limits<int64_t>::max)())))) > ((int64_t)((int64_t)9218868437227405312LL)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 double3_op_Implicit_m5DCE807570E8C929820AE8D221FFEE4861D9D5D9_inline (float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___v0, const RuntimeMethod* method) 
{
	{
		// public static implicit operator double3(float3 v) { return new double3(v); }
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_0 = ___v0;
		double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4 L_1;
		memset((&L_1), 0, sizeof(L_1));
		double3__ctor_m246C72AEDE9AC7E52CF7DF7FEE065D66EF96AB8A_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t BitConverter_DoubleToInt64Bits_m4F42741818550F9956B5FBAF88C051F4DE5B0AE6_inline (double ___value0, const RuntimeMethod* method) 
{
	{
		int64_t L_0 = *((int64_t*)((uintptr_t)(&___value0)));
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void double3__ctor_m246C72AEDE9AC7E52CF7DF7FEE065D66EF96AB8A_inline (double3_t4E22E063009822491E39D3E064709F4B4B9E6CF4* __this, float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E ___v0, const RuntimeMethod* method) 
{
	{
		// this.x = v.x;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_0 = ___v0;
		float L_1 = L_0.___x_0;
		__this->___x_0 = ((double)L_1);
		// this.y = v.y;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_2 = ___v0;
		float L_3 = L_2.___y_1;
		__this->___y_1 = ((double)L_3);
		// this.z = v.z;
		float3_t4AB5D88249ADB24F69FFD0793E8ED25E1CC3745E L_4 = ___v0;
		float L_5 = L_4.___z_2;
		__this->___z_2 = ((double)L_5);
		// }
		return;
	}
}
