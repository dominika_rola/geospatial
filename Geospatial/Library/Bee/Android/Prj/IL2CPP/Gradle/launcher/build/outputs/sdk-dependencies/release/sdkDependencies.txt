# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-places-placereport-16.0.0"
  }
  digests {
    sha256: "\004\370\272\353\037\217\212sL}K\027\001\243\227B\201\264U\221\257\372~\226;Y\335\001\233\212\274n"
  }
}
library {
  maven_library {
    artifactId: "com.android.support.support-core-ui-26.1.0"
  }
  digests {
    sha256: "\202\3658\005\025\2313^\250\201\354&D\aT|\253R\276u\017\026\316\t\234\373\'uO\307U\377"
  }
}
library {
  maven_library {
    artifactId: "arcore_client"
  }
  digests {
    sha256: "\260\032\355\230\231\033\3345q\350\f\023\233v\024\327x=\327\017\t\237\275\006@/x\2336\r\033\277"
  }
}
library {
  maven_library {
    artifactId: "com.android.support.support-fragment-26.1.0"
  }
  digests {
    sha256: "\240\2533i\357@\376\031\221`i/\004c\245\366?\022w\353\373d\335X|v\375\261(\327k2"
  }
}
library {
  maven_library {
    artifactId: "com.android.support.support-v4-26.1.0"
  }
  digests {
    sha256: "6\3308]\341\276w\221#\032\313\223;uq\230\371|\265;\307\320F\350\304\274@=!L\252\312"
  }
}
library {
  maven_library {
    artifactId: "UnityARCore"
  }
  digests {
    sha256: "\"\313R@\213O$\314\271\355\017\330\2726\263\n\270\333k\372\306M,\3305\025\033>Z\"\333\252"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-tasks-16.0.1"
  }
  digests {
    sha256: "\263\034\030\330\321\314\215\230\024\362\225\356t5G\0233\363p\272[\331\004\312\024\370\362\276\304\363\\5"
  }
}
library {
  maven_library {
    artifactId: "com.android.support.support-media-compat-26.1.0"
  }
  digests {
    sha256: "\235\214\356|\324\016\377\"\353\336\271\f\216p\365\356\226\305\275%\313,>;9@\342r\205\243\351\212"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-location-16.0.0"
  }
  digests {
    sha256: "$\n\017\313\236\216XXn8\352C\266\234\t\355n\211\352\232\fiw\vv4\330\035\253\365\363\240"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-base-16.0.1"
  }
  digests {
    sha256: "\254\241\fx\f2\031\274P\363\333\006sOJ\270\213\255\323\021<VL\n1V\377\217\366te["
  }
}
library {
  maven_library {
    artifactId: "android.arch.lifecycle.runtime-1.0.0"
  }
  digests {
    sha256: "\344\343N]\002\275\020.\2159\335\274)\371\352\330\241Za\343g\231=\002#\201\226\254HP\232\330"
  }
}
library {
  maven_library {
    artifactId: "com.android.support.support-compat-26.1.0"
  }
  digests {
    sha256: "}m\240\034\371vk\027\005\306\310\f\374\022\'J\211[@lL(y\000\260zV\024\\\246\3000"
  }
}
library {
  maven_library {
    artifactId: "ARPresto"
  }
  digests {
    sha256: "\225\354\266\vf\275\305\304\027#X\223\a\326;\313zR\310}-8>\306y\376B\300\026\330OE"
  }
}
library {
  maven_library {
    artifactId: "unityandroidpermissions"
  }
  digests {
    sha256: "\304\275\200/\267\017\253\2569\233,4\201\275\362\370\021i,\2045\246\3123t\313N\032\270\017h\272"
  }
}
library {
  maven_library {
    artifactId: "com.android.support.support-core-utils-26.1.0"
  }
  digests {
    sha256: "O\332mN\2640\227\036;\035\255tV\230\2033\363t\260\364\272\025\371\2309\312\032\n\265\025\\\212"
  }
}
library {
  maven_library {
    artifactId: "com.google.android.gms.play-services-basement-16.0.1"
  }
  digests {
    sha256: "\340\213\375\036\207\304\345\016\367aa\327\254v\270s\256\271u6~\353:\372J\276b\352\030\207\307\306"
  }
}
library {
  digests {
    sha256: "\231\326\031\232\325\240\232\016^\212I\244\314\b\370\030H=\334\375~\355\352/\231#A-\257\230#\t"
  }
}
library {
  digests {
    sha256: "Q\222\223L\327=\363.,\025r.\327\374H\215\336\220\272\256\311\256\003\000\020\335\032\200\373Nt\341"
  }
}
library {
  digests {
    sha256: "\263\v\320\224\326\345\377\316\253\363\337(\304\246\256\362\224/u\r\022\023\204\324B\'8S~vD\026"
  }
}
library {
  digests {
    sha256: "\206\2770\032 \255\f\320\243\221\342*R\346\373\371\005u\300\226\377\203#?\251\375\rR\263!\221!"
  }
}
library_dependencies {
}
library_dependencies {
  library_index: 1
}
library_dependencies {
  library_index: 2
}
library_dependencies {
  library_index: 3
}
library_dependencies {
  library_index: 4
}
library_dependencies {
  library_index: 5
}
library_dependencies {
  library_index: 6
}
library_dependencies {
  library_index: 7
}
library_dependencies {
  library_index: 8
}
library_dependencies {
  library_index: 9
}
library_dependencies {
  library_index: 10
}
library_dependencies {
  library_index: 11
}
library_dependencies {
  library_index: 12
}
library_dependencies {
  library_index: 13
}
library_dependencies {
  library_index: 14
}
library_dependencies {
  library_index: 15
}
library_dependencies {
  library_index: 16
}
library_dependencies {
  library_index: 17
}
library_dependencies {
  library_index: 18
}
library_dependencies {
  library_index: 19
}
module_dependencies {
  module_name: "base"
  dependency_index: 16
  dependency_index: 17
  dependency_index: 18
  dependency_index: 19
}
